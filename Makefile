PROJECT_NAME = puissance5

# _POSIX_C_SOURCE permet d'utiliser l'extension POSIX pour nanosleep
# car sinon il faut utiliser usleep, qui est déprécié
CC = gcc
CFLAGS = -Wall -std=c99 -D _POSIX_C_SOURCE=200809L
CLIBS = -lncurses

INTER_OBJ_CURSES = 	inter_interface_curses.o
INTER_OBJ_RAW = 	inter_interface_raw.o

IA_OBJ = 	ia_ia_minmax.o \
		ia_ia_random.o \
		ia_ia_alphabeta.o

SRC_OBJ = 	engine.o \
		grid.o \
		stack.o \
		utils.o

OBJ = $(IA_OBJ) $(SRC_OBJ)

all : out_raw out_curses

out_curses : $(OBJ) $(INTER_OBJ_CURSES)
	$(CC) $(CFLAGS) $(CLIBS) $(INTER_OBJ_CURSES) $(OBJ) -o $(PROJECT_NAME)-curses

out_raw : $(OBJ) $(INTER_OBJ_RAW)
	$(CC) $(CFLAGS) $(CLIBS) $(INTER_OBJ_RAW) $(OBJ) -o $(PROJECT_NAME)-raw

inter_%.o : src/interfaces/%.c
	$(CC) $(CFLAGS) -c $< -o $@

ia_%.o : src/ia/%.c
	$(CC) $(CFLAGS) -c $< -o $@

%.o : src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean :
	rm *.o

# Pour le rapport

REPORT_DIR = rapport
REPORT_SRC = rapport.tex
REPORT_OBJ = rapport-$(PROJECT_NAME).pdf

report : $(REPORT_DIR)/$(REPORT_SRC)
	cd $(REPORT_DIR) && latexmk -pdf $(REPORT_SRC)
	mv $(REPORT_DIR)/$(REPORT_SRC:.tex=.pdf) $(REPORT_OBJ)

clean-report :
	cd $(REPORT_DIR) && latexmk -c

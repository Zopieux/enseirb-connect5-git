                ┌─────────────────────────────────────┐
                │             Puissance 5             │
                ├─────────────────────────────────────┤
                │                                     │
                │ Projet programmation C / Semestre 5 │
                │           ENSEIRB-MATMECA           │
                │  ~ldauphin, ~amacabies, ~esagardia  │
                │                                     │
                └─────────────────────────────────────┘

│ Description │
└─────────────┘

Ce projet est écrit en langage C, standard C99. Il propose une interface
pseudo-graphique (ncurses) au jeu du "puissance 5".


│ Compilation │
└─────────────┘

La présence d'un Makefile permet de compiler le projet à l'aide d'une commande :
  $ make

Cela aura pour conséquence la création de deux binaires dans la racine du
projet (un par interface utilisateur) :
  - ./puissance5-raw     interface "brute" utilisant uniquement scanf() pour
                         interroger l'utilisateur
  - ./puissance5-curses  interface utilisant ncurses, plus jolie, se pilotant
                         au clavier avec les touches directionnelles.

Il est possible de générer le rapport au format PDF en utilisant
  $ make report
ce qui produit un fichier `rapport-puissance5.pdf` à la racine du projet.
L'utilitaire latexmk doit être installé sur le système pour que cette commande
fonctionne.


│ Utilisation │
└─────────────┘

AFFICHAGE BRUT (RAW)

  $ ./puissance5-raw

Suivre simplement les indications affichées au fur et à mesure.


AFFICHAGE CURSES

  $ ./puissance5-curses

Un menu graphique permet de sélectionner :
  - La taille de la grille ; attention, une taille trop élevée peut casser
    la disposition des éléments à l'écran et rendre le jeu difficile à suivre ;
  - Le type du premier joueur, typiquement un humain
  - Le type du deuxième joueur, par exemple une des deux IA (aléatoire ou
    intelligente) ou un second joueur humain.

La navigation dans le menu se fait à l'aider des flèches directionnelles
HAUT/BAS et le changement des paramètres avec les flèches DROITE/GAUCHE ou les
touches PLUS/MOINS.

Surligner "Lancer la partie !" et taper ENTREE pour commencer une partie.

Les joueurs humains sélectionnent une case à jouer à l'aide des flèches
directionnelles. Un appui sur ENTREE joue le pion à l'emplacement du curseur.

Il est possible de modifier le comportement du curseur en pressant la touche R
pour basculer entre :
  - curseur "libre" : le curseur est totalement libre et c'est au joueur de se
    placer sur une case disponible pour pouvoir valider son mouvement.
  - curseur "restreint" : le curseur est restreint aux cases où on peut jouer.

L'appui sur ECHAP permet à tout moment de quitter la partie en cours (ou le
programme, quand on est dans le menu).

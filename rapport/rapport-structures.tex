
L'écriture de ce projet en langage C a nécessité l'utilisation d'un certain
nombre de \emph{structures} pour contenir les données et les manipuler
proprement. Nous présentons ici ces structures et leur intérêt au sein du
programme.

\subsection{Configuration du jeu}

L'utilisateur a la possibilité de choisir la configuration du
jeu avant de commencer les parties et ce sans utiliser directement des options
en ligne de commande. Ce choix se justifie par le fait qu'il s'agit ici d'un
jeu et non d'un programme utilitaire comme GNU \code{grep} ou \code{find}. Cela
dit, il ne serait pas difficile de modifier ce comportement dans une autre
version du logiciel.

Pour contenir et communiquer cette configuration à travers les différentes
composantes (notamment entre l'affichage et le noyau), nous stockons les
paramètres de jeu dans une structure (fichier \code{src/const.h}):%

\vspace{-1em}\begin{verbatim}
struct Config {
   int grid_size;
   enum PlayerType player_type_a;
   enum PlayerType player_type_b;
}\end{verbatim}\vspace{-1em}%
%
où \code{PlayerType} permet de stocker le type de chaque joueur (humain ou IA):%

\vspace{-1em}\begin{verbatim}
enum PlayerType {HUMAN, RANDOM_IA, MINMAX_IA, ALPHABETA_IA}
\end{verbatim}\vspace{-1em}%

De cette manière, toutes les configurations énumérées dans l'introduction sont
réalisables sans ajouter de code particulier: humain/humain, IA/IA et humain/IA
sont traités par le même code, la seule différence se faisant lors de
l'acquisition de la position à jouer. Cela rend également le code modulaire:
pour rajouter une IA ``\code{bidule}'', il suffit d'ajouter \code{BIDULE_IA} dans
\code{PlayerType} et implanter son prototype \code{ia_bidule_play} dans le
fichier \code{src/ia/ia\textunderscore bidule.c}. Cette structure n'est en revanche pas
adaptée pour augmenter le nombre de joueurs, qui est fixé à deux \emph{en dur}
dans le programme.

\textbf{Complexité en espace:} il est immédiat qu'elle est \complexite{1}.

\subsection{Grille}

L'élément principal du jeu est le plateau où l'on pose les pions. Il s'agit
d'une simple grille carrée dont chaque case contient soit rien, soit le pion
d'un des deux joueurs. Nous stockons cette grille dans la structure suivante
(fichier \code{src/grid.c}):%

\vspace{-1em}\begin{verbatim}
struct Grid {
   int size;
   enum Position data[GRID_SIZE_MAX * GRID_SIZE_MAX];
}\end{verbatim}\vspace{-1em}%
%
avec \code{enum Position \{FORBIDDEN, AVAILABLE, PLAYER_A, PLAYER_B\}}.

Notons que nous utilisons un tableau \code{data} à une seule dimension. Il nous
a semblé plus simple de stocker la grille de cette manière plutôt que d'utiliser
une matrice, l'accès à la cellule en $\left(i, j\right)$ se faisant très
simplement: \code{grid.data[i * grid.size + j]}.

Après avoir développé certaines fonctions du jeu, nous nous sommes rendu compte
que cette structure méritait d'être étoffée avec d'autres champs. En effet, si
en théorie ces deux éléments suffisent, il s'est avéré plus efficace de mettre
en \og cache \fg{} d'autres données qui sont accédées à de nombreuses reprises.
Il s'agit:
\begin{itemize}
\item du carré de la taille de la grille, très utile dans les
itérations sur l'ensemble des cases et qu'il suffit de calculer une seule fois
à l'initialisation de la grille;
\item de la liste des cases sur lesquelles on peut jouer actuellement: cette
liste est simplement l'ensemble des cases de la grille qui sont
\code{AVAILABLE}. L'intérêt est de construire une seule fois ce tableau à chaque
pion posé, pour l'utiliser à loisir et sans surcoût dans les autres composantes
(l'affichage peut en avoir besoin pour mettre en valeur les cases disponibles et
les IA en ont besoin pour savoir où elles peuvent jouer).
\end{itemize}
La structure finale est donc:
\vspace{-1em}\begin{verbatim}
struct Grid {
   int size;
   int size_square;
   enum Position data[GRID_SIZE_MAX * GRID_SIZE_MAX];
   int available_pos[GRID_SIZE_MAX * GRID_SIZE_MAX];
   int available_count; // taille de available_pos
}\end{verbatim}\vspace{-1em}%
%
La lecture d'une case se fait grâce à la fonction
\code{grid_read_by_coords(grid, line, col)} ou
\code{grid_read_by_index(grid, index)} selon le contexte, sachant que là encore,
$index = line \times size + col$. L'écriture utilise les mêmes prototypes auxquels on
rajoute en dernier argument la valeur \code{Position} à écrire. Il est possible
de dupliquer une grille en mémoire avec \code{grid_copy} pour travailler sur
une copie sans modifier la grille principale, gérée par le noyau.
Par souci de concision et de simplicité, nous n'avons pas implanté de fonction
d'accès aux autres champs de la structure, qui sont donc publiquement
accessibles.

Remarquons que nous utilisons ici une allocation \emph{statique} pour le tableau
\code{data}, comme il a été préconisé lors de la présentation des projets. C'est,
dans ce cas précis, inutile de recourir à l'allocation dynamique car la taille
maximale de la grille est une constante du programme.

\textbf{Complexité en espace:} \complexite{\code{GRID\_SIZE\_MAX}^2}, sachant que
\code{GRID_SIZE_MAX} est fixé à la compilation.

\subsection{Pile}

Une pile d'entiers, limitée en capacité, est nécessaire dans l'implantation de
l'IA min-max et pour mémoriser l'alignement gagnant (qui peut être de 5 à 9
pions) en fin de partie afin de le mettre en valeur à l'écran.

L'implantation de cette pile est tout à fait classique, c'est pourquoi nous
n'allons pas nous y appesantir. Notons simplement que la pile est étoffée d'une
méthode permettant de la vider et d'une méthode permettant de choisir un élément
aléatoire dans cette pile. Fichier \code{src/stack.c}:
\vspace{-1em}\begin{verbatim}
struct Stack {
   int size;
   int data[GRID_SIZE_MAX * GRID_SIZE_MAX];
}\end{verbatim}\vspace{-1em}%
%
\textbf{Complexité en espace:} \complexite{\code{GRID\_SIZE\_MAX}^2}, sachant que
\code{GRID_SIZE_MAX} est fixé à la compilation.

\subsection{Point d'entrée et boucle principale}
Comme expliqué en introduction, le noyau interne est le point d'entrée (la
première fonction exécutée lors du lancement du programme) et c'est aussi ce qui
gère la boucle principale d'interaction pour enchaîner les menus, le placement
des pions et l'annonce de l'issue du match. Son flux d'instructions peut être
résumé par:

\begin{algorithm}[H]
	\Tq{Vrai}{
		Afficher_menu_de_configuration();\\
		partie = Initialiser_partie();\\
		\Tq{non Partie_est_terminée(partie)}{
			Afficher_plateau_de_jeu(partie);\\
			\eSi{Est_humain(partie.joueur_actuel)}{
				position = Demander_humain_où_jouer();
			}{
				position = Demander_IA_où_jouer();
			}
			Placer_pion(partie, position);\\
			gagnant = Déterminer_gagnant(partie);\\
			\eSi{gagnant}{
				Annoncer_gagnant_ou_match_nul(gagnant);\\
				Partie_terminée(partie);~~// va sortir de la boucle à la prochaine itération
			}{
				// Calcul des cases adjacentes\\
				Calculer_positions_jouables(partie);
			}
		}
	}
	\caption{Flux du programme principal}
\end{algorithm}

Par souci de clarté, ne figurent pas dans cet algorithme les méthodes permettant
à l'utilisateur de quitter le programme en sortant de la boucle \og{}infinie.\fg{}

\subsection{Détermination du gagnant}

L'une des tâches importantes que réalise le noyau est de déterminer si
le dernier coup joué a fait apparaître un gagnant ou un match nul. Insistons sur
le fait que la fonction en question, \code{get_winner}, ne permet pas de
déterminer, uniquement à partir d'une grille, si un joueur a gagné. Elle est
doit être appelée à chaque coup avec la position du dernier pion placé afin de
limiter la recherche au voisinage de ce dernier. Cette fonction se charge
également d'extraire l'alignement gagnant, s'il y en a un.

On suppose l'existence d'une fonction \code{pionAlentour} qui, étant donnés une
position $P$ dans la grille, un entier $k$ et une direction $d$ (ligne, colonne,
diagonale droite ou diagonale inverse), retourne l'identité du joueur ayant posé
le pion situé à la distance algébrique $k$ de $P$ dans la direction $d$.

\begin{algorithm}[H]
	\Entree{Dernier pion posé (position $P$); joueur propriétaire $J$}
	\Sortie{Identité du gagnant; alignement gagnant $A$ (si possible)}
	\Pour{$dir \in \left\{\text{Ligne}, \text{Colonne}, \text{Diagonale droite}, \text{Diagonale inverse}\right\}$}{
		$A \leftarrow pileVide()$\\
		empile($A$, $P$)\\
		$so\_far \leftarrow 1$\\
		$k \leftarrow 1$\\
		\Tq{$k \leq 5$ {\bf et} $pionAlentour\left(P, k, dir\right) = J$}{
			$k \leftarrow k + 1$\\
			$so\_far \leftarrow so\_far + 1$\\
			empile($A$, $pionAlentour\left(P, k, dir\right)$)
		}
		$k \leftarrow 1$\\
		\Tq{$k \leq 5$ {\bf et} $pionAlentour\left(P, -k, dir\right) = J$}{
			$k \leftarrow k + 1$\\
			$so\_far \leftarrow so\_far + 1$\\
			empile($A$, $pionAlentour\left(P, -k, dir\right)$)
		}
		\Si{$so\_far \geq 5$}{
			\Retour $J$, $A$
		}
	}
	\Retour Personne, pileVide()

	\caption{Détermination du gagnant (fonction \code{get_winner})}
\end{algorithm}
Cet algorithme, en résumé, recherche tous les alignements possibles qui
contiennent le pion tout juste joué et qui, bien-sûr, appartiennent au joueur
venant de jouer. Il a l'avantage de ne pas s'arrêter au premiers 5 pions
alignés: dans le cas où l'alignement gagnant est composé de plus de 5 pions
(illustré dans la figure \ref{fig:morethan}),
la pile $A$ contiendra bien l'ensemble de ces pions. À noter que la première
version de cet algorithme ne gérait pas ce dernier cas, auquel nous n'avions pas
pensé, et qui est heureusement apparu au cours d'une partie\footnote{Comme quoi,
réaliser des tests est d'une importance capitale car il est difficile
d'anticiper toutes les éventualités du premier coup.}.

\begin{figure}[h]
	\caption{Example d'alignement de plus de 5 pions \--- en supposant que O joue
	extrêmement mal et que X n'est pas pressé de gagner!}
	\label{fig:morethan}
	\centering
	\begin{tikzpicture}[
		scale=0.35,
		lab/.style={font=\footnotesize,anchor=center},
		cell/.style={font=\fontsize{5}{10}\selectfont,anchor=center}
	]
		\begin{scope}
			\node[anchor=center] at (1.5, 3.5) {\Large $\cdots$};
		\end{scope}
		\begin{scope}[xshift=4cm]
			\node[lab] at (3.5, -1) {Tour $n$};
			\node[cell] at (2.5, 0.5) {X};
			\node[cell] at (2.5, 1.5) {X};
			\node[cell] at (2.5, 2.5) {X};
			\node[cell] at (2.5, 3.5) {X};
			\node[cell] at (1.5, 0.5) {O};
			\node[cell] at (3.5, 0.5) {O};
			\node[cell] at (1.5, 1.5) {O};
			\node[cell] at (3.5, 1.5) {O};
			\draw (0, 0) grid (7, 7);
		\end{scope}
		\begin{scope}[xshift=12cm]
			\node[lab] at (3.5, -1) {Tour $n+2$};
			\draw[fill=playableColor] (1, 4) rectangle (2, 5);
			\draw[fill=playableColor] (2, 5) rectangle (3, 6);
			\node[cell] at (2.5, 0.5) {X};
			\node[cell] at (2.5, 1.5) {X};
			\node[cell] at (2.5, 2.5) {X};
			\node[cell] at (2.5, 3.5) {X};
			\node[cell] at (2.5, 5.5) {X};
			\node[cell] at (1.5, 0.5) {O};
			\node[cell] at (3.5, 0.5) {O};
			\node[cell] at (1.5, 1.5) {O};
			\node[cell] at (3.5, 1.5) {O};
			\node[cell] at (1.5, 4.5) {O};
			\draw (0, 0) grid (7, 7);
		\end{scope}
		\begin{scope}[xshift=20cm]
			\node[lab] at (3.5, -1) {Tour $n+4$};
			\draw[fill=playableColor] (3, 2) rectangle (4, 3);
			\draw[fill=playableColor] (2, 6) rectangle (3, 7);
			\node[cell] at (2.5, 0.5) {X};
			\node[cell] at (2.5, 1.5) {X};
			\node[cell] at (2.5, 2.5) {X};
			\node[cell] at (2.5, 3.5) {X};
			\node[cell] at (2.5, 5.5) {X};
			\node[cell] at (2.5, 6.5) {X};
			\node[cell] at (1.5, 0.5) {O};
			\node[cell] at (3.5, 0.5) {O};
			\node[cell] at (1.5, 1.5) {O};
			\node[cell] at (3.5, 1.5) {O};
			\node[cell] at (1.5, 4.5) {O};
			\node[cell] at (3.5, 2.5) {O};
			\draw (0, 0) grid (7, 7);
		\end{scope}
		\begin{scope}[xshift=28cm]
			\node[lab] at (3.5, -1) {Tour $n+5$};
			\draw[fill=playableColor] (2, 4) rectangle (3, 5);
			\node[cell] at (2.5, 0.5) {X};
			\node[cell] at (2.5, 1.5) {X};
			\node[cell] at (2.5, 2.5) {X};
			\node[cell] at (2.5, 3.5) {X};
			\node[cell] at (2.5, 5.5) {X};
			\node[cell] at (2.5, 6.5) {X};
			\node[cell] at (2.5, 4.5) {X};
			\node[cell] at (1.5, 0.5) {O};
			\node[cell] at (3.5, 0.5) {O};
			\node[cell] at (1.5, 1.5) {O};
			\node[cell] at (3.5, 1.5) {O};
			\node[cell] at (1.5, 4.5) {O};
			\node[cell] at (3.5, 2.5) {O};
			\draw (0, 0) grid (7, 7);
			\draw[very thick] (2, 0) rectangle (3, 7);
		\end{scope}
	\end{tikzpicture}
\end{figure}

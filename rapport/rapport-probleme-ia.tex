\subsection{L'IA aléatoire}

\textbf{Principe:} La machine va jouer dans n'importe quelle case disponible en la choisissant aléatoirement. La propension à remporter une partie sera évidemment très faible, à moins d'être face à un joueur au moins aussi \og{}bête.\fg{}

\textbf{Algorithme:} Cette partie fait réference au fichier \code{src/ia/ia_random.c}.
Nous y avons implanté une fonction \code{ia_random_play}.
La structure \code{Grid} permet d'accèder facilement aux cases où l'on peut effectivement jouer selon les règles du jeu. C'est pourquoi nous utilisons ici conjointement le tableau \code{grid$\rightarrow$available_pos} et sa taille \code{grid$\rightarrow$available_count} : on choisit aléatoirement un indice entre 0 et \code{grid$\rightarrow$available_count - 1} grâce à la fonction-outil \code{random_positive} et l'on retourne la position stockée à cet indice dans \code{grid$\rightarrow$available_pos}.

Remarque : le second argument de \code{ia_random_play} est le joueur actuel. Il n'est pas utilisé par l'algorithme mais par respect de l'API des intelligences artificielles, cet argument \og inutile \fg{} est maintenu.

\textbf{Complexité en temps:} On peut raisonnablement supposer que la fonction \code{rand} de la librairie standard C renvoie une valeur en temps constant, la fonction \code{ia_random_play} a donc une complexité constante \complexite{1}.

\subsection{L'IA minmax}

\textbf{Principe:} L'IA minmax va jouer dans la case qui permet à la grille d'obtenir un score maximal pour un nombre donné de coups anticipés. Évidemment, un score maximal correspond à un coup \og intelligent.\fg{} Le nombre de coups anticipés est assimilé à une \emph{profondeur} de recherche (il s'agit donc du nombre de pions posés par les deux joueurs) et le score est calculé grâce à une fonction d'évaluation que nous détaillons ci-après.


\subsubsection{Fonction d'évaluation}

\textbf{Problème:} Nous voulons qu'à partir de la grille de jeu, on donne une \og note \fg{} qui donne une idée de la situation pour un joueur donné. Plus la note est haute, meilleure est la situation pour le joueur.

\textbf{Analyse:} Il faut donc définir ce que veut dire une \og bonne ou une mauvaise situation \fg{} pour un joueur. Hormis les cas triviaux où l'un des joueurs a un alignement de (au moins) cinq pions et donc la note sera minimale ou maximale (selon le gagnant), l'évaluation d'une configuration de jeu n'est pas évidente. Nous avons choisi de donner une note proportionnelle au carré de la puissance de deux du plus grand alignement du jeu. Cela permet de mettre en évidence les cas critiques comme un alignement de quatre pions sans pion adverse au bout. Les alignements de l'IA ont une valeur positive et ceux de l'adversaire ont une valeur négative. Cela met en valeur un sous problème: comment détecter les alignements?

\textbf{Principe:} Nous découpons la grille de taille \code{size_square} en \code{size_square} grilles de tailles $5 \times 5$ pour lesquelles nous évaluons le nombre de pions alignés pour le joueur courant sur les deux diagonales, la ligne mediane et la colonne médiane, sous une certaine condition: dès qu'un pion adverse se trouve sur l'alignement, cet alignement n'est pas pris en compte. Nous retenons ensuite le nombre maximal de pions alignés que nous portons en l'exposant d'une puissance de 2. Cette évaluation étant faite pour chaque grille $5 \times 5$ ayant pour case centrale chaque case de la grille, nous retenons, comme score global, le score maximal parmi les petites grilles, élevé au carré par souci de différenciation petite grille / plateau de jeu.
Ce principe est schématisé par un exemple sur les figures \ref{fig:eval}, \ref{fig:T} et \ref{fig:boolean}.
\input{schema-eval}

\textbf{Algorithme:} Cette partie fait réference au code dans \code{src/ia/ia_minmax.c}, pour les fonctions \code{tiny_eval} et \code{ia_minmax_evaluation}.
La fonction \code{tiny_eval} renvoie le score de la petite grille comme définie précedemment. Pour cela, elle teste quatre alignements de cinq cases chacun: l'alignement horizontal, vertical et le deux diagonales contenant la case centrale. On parcourt donc la grille $5 \times 5$ \og en étoile \fg{} (du centre aux extrémités), retenant le nombre de pions présents sur ces alignements. Si des pions des deux joueurs sont sur un même alignement, son score est nul. Sinon, son score est égal à la puissance de deux du nombre de pions présents sur cet alignement (positif ou négatif selon le joueur qui possède ces pions).
La fonction \code{ia_minmax_evaluation} appelle \code{tiny_eval} sur chaque case de la grille. Ensuite, elle renvoie le maximum atteint par la meilleure grille $5 \times 5$ élevé au carré et postif pour le joueur courant, négatif pour le joueur adverse, d'où la condition finale.

\begin{algorithm}[H]
  \caption{Evaluation}
  \Entree{grille: $Grille$, joueur: $Joueur$}
  \Sortie{Nombre entier}
  $eval_{cur} \leftarrow 0$\\
  $eval_{max} \leftarrow 0$\\
  $eval_{min} \leftarrow 0$\\
  \Pour{$ case \in grille $}
  {
    $eval_{cur} \leftarrow $ \code{tiny_eval} $(grille, case, joueur)$\\
    $eval_{max} \leftarrow max(eval_{max}, eval_{cur})$\\
    $eval_{cur} \leftarrow min(eval_{min}, eval_{cur})$\\
  }

  \Si{$\left|eval_{min}\right| \leq \left|eval_{max}\right|$}
  {
    \Retour $eval_{max}^2$
  } \Sinon {
    \Retour $eval_{min}^2$
  }
\end{algorithm}


\textbf{Complexité en temps:}
\begin{itemize}
\item La fonction \code{tiny_eval}\\
Pour la première boucle, on effectue le parcours d'un tableau de taille 4; les trois boucles imbriquées, on parcours les cases des deux diagonales, des ligne et colonne médianes, soit 17 cases; pour la dernière boucle, on parcours à nouveau un tableau de taille 4. La complexité de la fonction \code{tiny_eval} ne dépend donc d'aucun des paramètres en entrée. Elle est de complexité constante \complexite{1}.
\item La fonction \code{ia_minmax_evaluation}\\
Elle appelle \code{tiny_eval}, de complexité constante donc, pour toutes les cases de la grille (boucle de 0 à \code{size_square}). Elle est donc de complexité linéaire \complexite{\code{size\_square}}.
\end{itemize}

\subsubsection{L'algorithme minmax}

\textbf{Principe:} Le principe de l'algorithme minmax peut être visualisé sous forme d'arbre. La profondeur correspond au nombre de coups joués qui sont anticipés. Ainsi, pour une profondeur \code{max_depth}, on pose virtuellement \code{max_depth - 1} pions, chaque joueur à tour de rôle; toutes les configurations possibles sont envisagées. Arrivé à ce stade, on pose un dernier pion virtuellement dans une case disponible, on calcule le score de la grille, on retire le pion et on le pose sur une autre case disponible, puis on calcule le score de cette nouvelle grille, ainsi de suite en posant le pion dans chaque case disponible, en retenant alors le score maximal.\\
En se réferant à la représentation arborescente de l'algorithme. La profondeur \code{max_depth - 1} comporte un certain nombre de n\oe uds correspondant à toutes les façons de poser les \code{max_depth - 1} n\oe uds depuis la grille initiale. \`A partir de là, le fait de poser un pion tour à tour dans chaque case disponible va génerer autant de fils pour chaque n\oe ud, on fait alors remonter le score maximal dans le n\oe ud père.
Une fois que les maxima ont été calculés pour une profondeur \code{max_depth - 1}, le principe est de remonter cette fois le minimum de ces scores au père de ces n\oe uds.
Ainsi, en évaluant le score des grilles à la profondeur maximale, on remonte les score jusqu'au n\oe ud initial en alternant maximum / minimum et on retient alors l'indice qui nous permet d'obtenir le score maximal sous ces conditions.
Le principe est représenté à la figure \ref{fig:minmax}.

\input{schema-minmax}

\textbf{Algorithme:} Cette partie fait réference au code \code{src/ia/ia_minmax.c} pour les fonctions \code{ia_minmax_rec_play} et \code{ia_minmax_play}.

\begin{itemize}
\item La fonction \code{ia_minmax_rec_play}\\
La condition génerale \code{if(depth < max_depth)} permet de différencier le cas de profondeur maximum où on calcule effectivement les scores avec la fonction d'évaluation, du reste des profondeurs où on remonte les scores en les minimisant ou maximisant en alternant.
Pour la partie génerale de l'algorithme où s'effectue la remontée, la condition \code{if(depth >= 1)} échange le joueur quand on change de profondeur, ce qui renvoie bien à l'alternance de pions posés par le joueur A puis le joueur B.
La boucle de 0 à \code{grid$\rightarrow$available_count} correspond au fait de poser successivement les pions dans chaque case disponible ou encore à générer les fils pour chaque n\oe ud de l'arbre. On utilise ici de l'allocation dynamique avec la fonction \code{grid_copy} pour jouer virtuellement dans une copie de la grille, sans altérer le plateau de jeu.

On calcule alors récursivement le score jusqu'à atteindre la profondeur \code{max_depth} (d'où la condition initiale).
Ainsi, une fois le score calculé, on retient soit le score minimum soit le score maximum en fonction de la profondeur: condition \code{depth mod 2 != max_depth \% 2}. Le cas particulier où le score maximum est atteint est traité à part \code{if(cur == MAX_VAL \&\& max_depth <= 2)}, en effet dès qu'il est atteint, il devient inutile de comparer les scores suivants. D'autre part, quand plusieurs positions permettent d'obtenir le même score, on utilise une pile \code{rand_pile} qui va retenir les indices de ces positions là pour permettre finalement d'utiliser un choix aléatoire (\code{if(!stack_is_empty(rand_pile))}) dans le cas où le score retenu est obtenu à partir de plusieurs indices différents. Le but est d'éviter de faire apparaître un scénario de jeu répétitif (typiquement, \og je joue sur la case la plus à gauche et la plus en haut \fg{} comme le faire un algorithme non-aléatoire \og je joue sur la première case disponible \fg{}).

Il nous reste alors à expliquer le rôle des autres valeurs d'entrée. Nous choisissons d'utiliser des pointeurs pour l'indice et le score pour qu'ils soient disponibles après l'exécution de le fonction. Précisement, c'est l'indice dans lequel nous devons jouer pour maximiser notre score, qui reste la donnée la plus importante et donc qui doit être disponible en dehors de la fonction \code{ia_minmax_rec_play}. Enfin, le pointeur vers le booléen \code{continue_rec}, va permettre de savoir en dehors de \code{ia_minmax_rec_play} si le score maximal a été atteint ou non, ce qui sera important pour la suite.

\item La fonction \code{ia_minmax_play}\\
On utilise une boucle \emph{tant que} qui va utiliser la fonction \code{ia_minmax_rec_play} à plusieurs profondeurs jusqu'à atteindre \code{MAX_DEPTH}, modifiable à la compilation comme variable préprocesseur. La boucle s'arrêtera aussi lorsque le booléen \code{continue_rec} sera faux, ce qui correspondra au fait que l'un des joueurs a gagné. Il sera alors inutile d'anticiper un nombre de coup supérieur.

\end{itemize}


\textbf{Complexité en temps:} La fonction \code{ia_minmax_rec_play} utilise une boucle \emph{pour} et est récursive, ce qui nous amène à une formule de récurrence du type $f(depth, size\_square) = grid\rightarrow{}available\_count \times f(depth - 1, size\_square) + size\_square$. Or, \code{grid$\rightarrow$available_count} dépend de la profondeur. Elle peut être minorée par 1 et majorée par \emph{size_square}. En poussant la réflexion un peu plus loin, vues les conditions posées pour les règles du jeu (on ne peut jouer que dans une case adjacente à une case déjà occupée), le pire cas serait la grille où les cases d'indices paires sont occupées et les cases d'indices impaires jouables, ou vice-versa, c'est à dire que les cases ne seraient adjacentes qu'en diagonales. Alors, nous pouvons majorer \code{case_available_count} par $frac{size\_square}{2}$. La formule de récurrence est alors de la forme $f(depth, size\_square) = \frac{size\_square}{2} \times f(depth-1, size\_square)$. Nous allons résoudre cette suite de type arithmético-géométrique.

\begin{equation*}
\begin{split}
 m   & = \code{size\_square} \\
 n   & = \code{DEPTH\_MAX} \\
 u_n & = \frac{m}{2} \times u_{n-1} + m \\
 u_0 & = 1 \text{  car grille de taille 1, pas de choix de l'indice.} \\
 u_n & = (\frac{m}{2})^{n} \times (1-\frac{m}{1-\frac{m}{2}}) + \frac{m}{1-\frac{m}{2}}
\end{split}
\end{equation*}

La complexité est donc en \complexite{ (\frac{m}{2})^{n} \times (1-\frac{m}{1-\frac{m}{2}}) + \frac{m}{1-\frac{m}{2}} } = \complexite{m^n} = \complexite{\code{size\_square}^\code{DEPTH\_MAX}}.

\subsection{L'IA alpha-bêta}

\textbf{Principe:} Alpha-bêta n'est qu'une amélioration de minmax en terme de rapidité. En effet, le fait d'alterner le choix minimum et le choix maximum va nous permettre de faire moins de comparaisons, ce qui revient à \emph{élaguer} l'arbre représentatif de l'algorithme minmax.

Revenons au principe de minmax. Nous sommes à la profondeur \code{max_depth - 1}, nous avons calculé le maximum des feuilles du premier père tout en bas de l'arbre, nous retenons cette valeur comme $\alpha$. Nous passons au deuxième n\oe ud de profondeur \code{max_depth - 1}; dans le cas où un de ses fils serait supérieur à $\alpha$, il ne sert à rien de continuer les comparaisons. Le père prend cette valeur et nous ne calculons pas la valeur des autres fils. En effet, rappelons que le choix vient justement de l'alternance minmax. \`A la profondeur \code{max_depth - 2}, nous remonterons le minimum. Donc dès que nous dépassons $\alpha$, nous pouvons garantir que la valeur de ce n\oe ud ne sera pas remonter en \code{max_depth - 2} puisque la valeur de $\alpha$ lui sera préferé.
Par ailleurs, si quand nous passons au deuxième n\oe ud de profondeur \code{max_depth -1}, nous ne dépassons pas $\alpha$ et donc que le score remonté est plus petit que $\alpha$, $\alpha$ est affecté de ce nouveau score.
Nous pouvons alors continuer de parcourir les n\oe uds de profondeur \code{max_depth - 1} en respectant l'élagage $\alpha$.
L'élagage $\beta$ est analogue pour les profondeurs où l'on remonte le \textbf{minimum}. En effet, $\beta$ sera la valeur seuil \textbf{en-dessous} de laquelle on élaguera.
Le principe est illustré à la figure \ref{fig:alphabeta}.

\input{schema-alphabeta}

\textbf{Algorithme:} Nous faisons ici réference au fichier \code{src/ia/ia_alphabeta.c}.
Au vu de ce que nous avons expliqué, alpha-bêta n'est qu'une amélioration de minmax et donc, les fonctions \code{tiny_eval} et \code{ia_minmax_evaluation} sont réutilisées.
La fonction \code{ia_alphabeta_rec_play} est très similaire à la fonction \code{ia_minmax_rec_play}. $\alpha$ et $\beta$ jouent le rôle de score respectivement dans les transitions min et max, les valeurs seuils (au sens détaillé précedemment) sont alors $a$ et $b$. Les conditions \code{if(a >= beta)} et \code{if(alpha >= b)} permettent alors l'élagage: une valeur est retournée à l'intérieur de la boucle: le parcours s'arrête.
 Enfin, la fonction \code{ia_alphabeta_play} fait appel à \code{ia_aplhabeta_play_rec} de la même façon que \code{ia_minmax_play} appelait \code{ia_minmax_play_rec}. L'efficacité suffisante de l'algo minmax sans élagage nous pousse de plus à utiliser \code{ia_minmax_rec_play} quand les profondeurs \code{max_depth} sont inférieures ou égales à 2.

\textbf{Complexité en temps:} L'élagage alpha-bêta améliore expérimentalement la rapidité de l'IA. La complexité est forcément inférieure à celle de l'algorithme minmax mais difficilement quantifiable. En effet, les cas où la boucle \emph{pour} est interrompue (élagage) ne sont pas quantifiables simplement au vu d'une taille de grille ou de la profondeur envisagée. Nous avons par ailleurs vu que sur les petites profondeurs, l'amélioration était inintérressante.
Nous nous contenterons donc d'affirmer que la complexité est toujours de \complexite{ (\frac{m}{2})^{n} \times (1-\frac{m}{1-\frac{m}{2}}) + \frac{m}{1-\frac{m}{2}} } mais que pour une grille et une profondeur donnée assez grande, l'algorithme avec élagage apparait comme de complexité plus faible.

\subsection{Tests des intelligences artificielles}

Les phases de tests sont constituées de 20 tests, chacun sur des grilles $13 \times 13$.

\subsubsection{IA contre humain}

L'IA aléatoire, bien que très rapide, perd à tous les coups contre un humain jouant \og au mieux. \fg{}

L'IA minmax a une efficacité relative à la profondeur \code{max_depth}
Pour une profondeur 1, l'IA est facilement déjouable, car elle ne bénéficie d'aucune anticipation et donc ne se défend pas sur une case où l'adversaire pourrait gagner au coup suivant. Elle se contente d'aligner le plus possible ses pions sans prendre en compte l'adversaire, ce qui s'explique par ailleurs avec l'algorithme. En effet, il n'y a de test que sur le pion posé par le joueur lui-même et donc aucune évaluation n'est faite sur le pion posé par le joueur suivant.

Pour une profondeur 2, l'IA bloque sans arrêt les alignements adverses et néglige quelque peu ses propres possibilités de victoire sauf dans le cas où elle peut gagner en posant un seul pion. L'algorithme en profondeur 2 consite bien à minimiser le score adverse, ce qui relève bien d'une stratégie défensive. Cette dernière est ici difficilement mise en échec.

Pour une profondeur 3, l'IA gère de front le fait de défendre en bloquant l'adversaire et de tenter d'aligner des pions. De ce fait, elle a plus de chance de gagner mais et plus facilement attaquable sur sa défense. Cependant, bien que la réponse de l'IA sur des coups où elle peut gagner où empecher l'autre de gagner soit prompte (algorithme toujours lancé en profondeur 1 et 2), elle devient assez lente sur les coups géneraux.

D'où l'interêt d'utiliser alors l'IA alpha-bêta, qui renvoie le même indice que l'IA minmax aurait renvoyé en profondeur 3 mais un peu plus rapidement.

L'IA minmax en profondeur 2 nous semble donc être le mode ayant le meilleur rapport \og intelligence / lenteur \fg {} si on considère une stratégie de défense au plus près.

\subsubsection{IA contre IA}

Faire jouer l'IA aléatoire contre elle-même donne les résultats suivants:

\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
A & B & A & B & B & B & A & B & B & A & A & B & A & A & A & A & A & A & B & B \\
36 & 50 & 32& 48 & 38 & 39 & 51 & 40 & 86 & 11 & 65 & 26 & 33 & 55 & 15 & 83 & 73 & 57& 62 & 41 \\
\hline
\end{tabular}

Ce qui fait 9 victoires de B et 11 de A avec une moyenne de 47 coups joués.
Le résultat était envisageable, les IA n'ayant aucune stratégie, rien ne justifiait une plus grande probabilité de gain pour un joueur que pour l'autre.

Les tests IA aléatoire contre IA minmax en profondeur 2 sont tous à l'avantage de l'IA minmax avec une moyenne de 30 à 40 coups joués au total.

Les tests effectués entre l'IA aléatoire contre l'IA minmax en profondeur 3 sont également à l'avantage de l'IA minmax, toujours avec une moyenne de 30 à 40 coups joués au total, le temps de réponse ayant en revanche légèrement augmenté.
On observe donc une légere baisse du nombre moyen de coup joué et, évidemment, la stratégie de l'IA minmax est gagnante. Les observations faites au niveau du mode IA contre humain ne sont pourtant pas retrouvées ici quant à l'influence de la profondeur.

Cependant, la \og bêtise \fg {} de l'IA aléatoire ne permet pas vraiment d'interpréter le comportement d'une IA.

Les tests IA minmax contre alpha-bêta de même profondeur 2 (lancés sur seulement 15 tours à cause de la lenteur) donne les résultats suivants: 12 matchs nuls, 1 gain de A en 40 coups, 2 gains de B en 150 et 85 coups.

Avec des profondeurs différentes (A en profondeur 3 et B en profondeur 2), on obtient 14 victoires de B et une de A sur une moyenn de de 70 coups.
L'IA en profondeur 2 est donc plus puissante d'après ce dernier test et par ailleurs, le nombre de coups moyens pour gagner est élevé, ce qui confirme l'hypothèse d'une configuration défensive qui gagne sur opportunité. Les 12 matchs nuls de la phase 1 confirment cette idée de défense qui dure souvent jusqu'à remplir la grille.

En conclusion, nous recommandons l'IA minmax en profondeur 2 (c'est la valeur du \code{define} par défaut) pour le meilleur compromis entre rapidité et habileté des coups joués.

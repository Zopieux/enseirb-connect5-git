Nous avons choisi, comme expliqué en partie \ref{sec:probleme}, d'isoler
l'interface utilisateur (ou interface graphique) du reste du programme au moyen
d'une véritable API. Pour illustrer l'intérêt de ce choix, nous avons réalisé
deux interfaces distinctes: une première à l'aspect visuellement agréable,
utilisant la librairie ncurses, et une autre plus simple mais moins pratique à
utiliser. Ces deux interfaces sont illustrées respectivement dans les figures
\ref{fig:sc-game} et \ref{fig:sc-raw}.

\begin{figure}[ht]
\begin{minipage}[b]{0.62\linewidth}
\centering
\includegraphics[width=\textwidth]{interface-sc-game}
\caption{Capture d'écran de l'interface ncurses pendant une partie}
\label{fig:sc-game}
\end{minipage}
\hspace{0.3cm}
\begin{minipage}[b]{0.28\linewidth}
\centering
\includegraphics[scale=0.6]{interface-sc-raw}
\caption{Capture d'écran de l'interface \og brute \fg{} pendant une partie}
\label{fig:sc-raw}
\end{minipage}
\end{figure}

\subsection{Cahier des charges d'une interface}

L'API impose aux interfaces graphiques d'implanter:
\begin{itemize}
\item un menu de configuration,
dans lequel l'utilisateur fixe certains paramètres (comme vu dans la partie
\emph{structures}) comme la taille de la grille et les joueurs;
\item un rendu de la grille, afin de suivre le déroulement du match;
\item une manière de séléctionner, pour un joueur humain, le pion qu'il souhaite
jouer au tour suivant;
\item enfin, un affichage de l'issue du match (gagnant ou match nul).
\end{itemize}
Les prototypes C des fonctions correspondantes permettent une souplesse
suffisante pour que des interfaces de tout type puissent être implantées. Sont
par exemple prévues des fonctions d'initialisation et de nettoyage, appelées
respectivement au lancement et à la fin du programme. Par ailleurs, ladite
fonction d'initialisation a la possibilité de retourner une
structure\footnote{Plus précisement, un pointeur vers cette structure.} qu'elle
aura définit dans son fichier \code{.c} et qui lui permettra de communiquer des
objets utiles dans les autres fonctions. Cette structure est par exemple
utilisée dans l'interface ncurses pour garder une référence vers les différentes
\og fenêtres \fg{} qui composent l'interface de jeu ainsi qu'un certain nombre
de tailles, pour aligner convenablement les éléments sur l'écran.

\subsection{Interface ncurses: quelques détails}

Il est intéressant de s'attarder quelque peu sur l'interface réalisée avec la
librairie ncurses. Cette librairie étant à la fois pratique mais assez pauvre
(bas niveau, dirons-nous), il faut parfois recourir à certaines astuces pour
arriver à nos fins.

\paragraph*{Structure générique d'une UI}
Comme on peut le constater dans \code{interface_curses.c}, les fonctions
d'interaction avec l'utilisateur suivent toutes la même structure générale: il
s'agit d'une boucle d'interaction qui attend un \og mouvement \fg{} de
l'utilisateur pour passer à l'étape (ou à l'écran) suivante.

\begin{algorithm}[H]
	\Tq{Vrai}{
		RenduÉcran();\\
		(caractère) touche $\leftarrow$ RécupérerToucheClavier();\\
		\Si{EstTouchePourQuitter(touche)}{
			\textbf{quitter boucle}
		}
		(booléen) écran_terminé $\leftarrow$ TraitementActions(touche);\\
		\Si{écran_terminé}{
			\textbf{quitter boucle}
		}
	}
	\caption{Structure générique d'une UI ncurses}
\end{algorithm}
On constate qu'une interface très simple comme ncurses est régie par le même
genre de boucle d'interaction que des UI plus évoluées. La différence avec des
librairies plus riches comme Qt est que ces dernières peuvent gérer
un nombre bien plus important d'évènements, comme des clics de la souris, des
minuteurs, le déplacement ou redimensionnement de la fenêtre, etc. Il devient
alors indispensable de programmer avec des concepts de déclencheurs et de
\emph{callbacks}, à l'inverse de ncurses dont les appels sont bloquants.

\paragraph*{\emph{Timeout} pour \code{getch}}
Pour donner un peu de vie au menu de paramétrage de l'interface, nous avons
implanté une animation ASCII sur le curseur permettant de mettre en valeur
l'option en cours de modification. Cette animation affiche un caractère à gauche
et à droite de la ligne en cours, qui cycle périodiquement. ncurses n'est pas
conçue pour une programmation événementielle; l'appel de \code{getch} pour récupérer
les pressions clavier est \emph{bloquant}. Comment est-il possible, dès lors,
d'afficher cette animation tout en laissant l'utilisateur utiliser l'interface ?

La solution réside dans l'utilisation de la fonction \code{timeout} qui, comme
son nom l'indique plus ou moins, permet à la fonction \code{getch} d'attendre
un certain temps (et pas indéfiniment) avant de retourner. En choisissant une
durée de \emph{timeout} assez faible \--- de quelques dixièmes de secondes \---
on peut alors enchaîner l'animation jusqu'à ce que \code{getch} retourne un vrai
code de touche, et pas \code{ERR} comme c'est le cas lorsque la fonction
\emph{timeout}. La durée du minuteur est \code{MENU_TICKRATE} dans
\code{src/interfaces/interface_curses.c}, c'est le temps entre deux \emph{frames}
de l'animation. On notera qu'il faut penser à supprimer ce minuteur dès que l'on
sort du menu, en donnant une valeur négative à la fonction \code{timeout}.

\paragraph*{Constantes numériques}
La première chose qui frappe l'esprit dans \code{interface_curses.c}
est certainement le nombre important de constantes préprocesseur, la plupart
numériques, déclarées en début de fichier. L'utilisation de ces constantes est
cruciale pour éviter d'écrire des nombres en dur dans les fonctions ncurses
d'affichage de chaînes: la signification de ces chiffres serait rapidement incertaine,
d'autant que la plupart sont utilisées un grand nombre de fois afin d'obtenir
une interface homogène entre les différents écrans (typiquement, les marges).
La grande quantité de ces constantes est inhérente à la construction d'une
interface graphique, pour laquelle les éléments (textes, menus, curseurs, etc.)
se doivent d'être correctement alignés afin d'obtenir une interface intuitive
et esthétiquement agréable. Malgré tout, il est difficile de se passer
totalement de la présence de petits \emph{offsets} çà et là, comme des \code{-1}
ou des \code{+2}, car il est parfois nécessaire d'afficher des textes formatés
et la longueur des codes de formatage (tels que \code{\%s} ou \code{\%d})
doivent donc être soustraits à la taille de la chaîne de caractères finale. On
comprend bien qu'il devient inutilement verbeux d'utiliser des constantes
préprocesseur pour ces chiffres, même si la lisibilité code s'en retrouve
légèrement impactée. Un commentaire concis dans le code permettra alors de palier
le problème.

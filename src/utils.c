#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int
max_int(int a, int b) {
    return (a > b) ? a : b;
}


int
min_int(int a, int b) {
    return (a < b) ? a : b;
}


int
abs_int(int a) {
    return (a < 0) ? - a : a;
}


int
random_int(int min, int max) {
    return rand() % (max - min + 1) + min;
}


int
random_positive(int max) {
    return random_int(0, max);
}


void msleep(unsigned long msec) {
    struct timespec req;
    time_t sec = (int)(msec / 1000);
    msec = msec - sec * 1000;
    req.tv_sec = sec;
    req.tv_nsec = msec * 1000000L;
    while(nanosleep(&req, &req) == -1)
        continue;
}

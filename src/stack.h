/*
Une simple d'une pile d'entiers, limitée en capacité par le carré
de la taille maximale de la grille.

L'implémentation se veut minimaliste, sans vérification de dépassement.
Les prototypes parlent d'eux-même.
*/

#ifndef STACK_H_
#define STACK_H_

#include <stdbool.h>
#include "const.h"

typedef struct Stack Stack;

Stack* stack_create();
void stack_destroy(Stack*);
Stack* stack_clear(Stack*);
Stack* stack_push(Stack*, int);
int stack_pop(Stack*);
int stack_head(Stack*);
int stack_random_elem(Stack*);
bool stack_is_empty(Stack*);

#endif

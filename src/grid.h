/*
Structure de donnée de la grille et méthodes correspondantes.
*/

#ifndef GRID_H_
#define GRID_H_

#include "const.h"

struct Grid {
    int size;
    int size_square; // accédé souvent !
    enum Position data[GRID_SIZE_MAX * GRID_SIZE_MAX];
    // Tableau de "cache" des positions dispo
    int available_pos[GRID_SIZE_MAX * GRID_SIZE_MAX];
    int available_count;
};

// Initialise une grille de taille donnée
void grid_init(struct Grid* grid, int size);

// Met à jour le cache des positions disponibles
void grid_update(struct Grid*);

// Accède à l'indice (line, column) de la grille donnée
enum Position grid_read_by_coords(struct Grid* grid, int line, int column);

// Accède à l'indice "plat" de la grille donnée
enum Position grid_read_by_index(struct Grid* grid, int index);

// Ecrit la valeur à l'indice (line, column) de la grille donnée
void grid_write_by_coords(struct Grid* grid, int line, int column,
    enum Position pos);

// Ecrit la valeur à l'indice "plat" de la grille donnée
void grid_write_by_index(struct Grid* grid, int index, enum Position pos);

// Copie la grille from dans dest
void grid_copy(struct Grid* dest, struct Grid* from);

#endif

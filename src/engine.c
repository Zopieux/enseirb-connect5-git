#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include "const.h"
#include "grid.h"
#include "ia/ia.h"
#include "interfaces/interface.h"
#include "stack.h"
#include "utils.h"


#define CELL_UP(tab, k, i, j) do { tab[k][0] = i; tab[k][1] = j; } while(0)


static enum Winner get_winner(struct Grid*, int, Stack*);


// Fonction auxiliaire pour get_winner
static void
get_winner_test(
    struct Grid* grid,
    int line, int col, int* so_far, enum Position current_player,
    int line_multiplier, int col_multiplier,
    Stack* winning_moves
) {
    int k = 1;
    int c_line, c_col;
    /*
    Black magic... just kidding.
    Cette boucle essaye d'avancer dans la direction donnée par
    col/line_multiplier tant que l'on est sur le même joueur. Ainsi, si l'on
    atteint PUISSANCE_N (= 5) pions du même joueur, celui-ci a gagné (ie. so_far
    vaut PUISSANCE_N).
    On aurait pu faire tous les tests dans la condition du while mais cela
    rendait le code très verbeux (et redondant) à cause des multiples calculs
    de c_line et c_col, ici réalisés une seule fois.
    */
    while(true) {
        c_line = line + line_multiplier * k;
        c_col = col + col_multiplier * k;
        if(c_line < 0 || c_col < 0 ||
            c_line >= grid->size || c_col >= grid->size ||
            grid_read_by_coords(grid, c_line, c_col) != current_player)
            break;
        k++; (*so_far)++;
        stack_push(winning_moves, c_line * grid->size + c_col);
    }
}


// Recherche si le joueur a gagné
static enum Winner
get_winner(struct Grid* grid, int last_index, Stack* winning_moves) {
    int
        line = last_index / grid->size,
        col = last_index % grid->size;

    static const int directions[4][2][2] = {
        {{ 0, -1}, { 0, +1}}, // en ligne, gauche et droite
        {{-1,  0}, {+1,  0}}, // en colonne, bas et haut
        {{-1, +1}, {+1, -1}}, // en diagonale, vers haut droite et bas gauche
        {{-1, -1}, {+1, +1}}  // en diagonale, vers bas droite et haut gauche
    };

    enum Position last_pos = grid_read_by_index(grid, last_index);

    int so_far, dir, i;

    /*
    Pour éviter la répétition de code, on stocke les différents multiplicateurs
    en ligne en colonne dans un tableau (ci-dessus) et on boucle desssus.
    */
    for(dir = 0; dir < 4; dir++) {
        so_far = 1;
        stack_clear(winning_moves);
        // le pion tout juste joué fait évidemment partie de l'alignement
        stack_push(winning_moves, last_index);
        for(i = 0; i < 2; i++) {
            get_winner_test(grid, line, col, &so_far, last_pos,
                directions[dir][i][0], directions[dir][i][1],
                winning_moves);
        }
        if(so_far >= PUISSANCE_N)
            return (last_pos == PLAYER_A) ? PLAYER_A_WIN : PLAYER_B_WIN;
    }

    return NOBODY;
}


static void
random_seed() {
    // Seed de rand() avec le temps
    int stime;
    long ltime;
    ltime = time(NULL);
    stime = (unsigned) ltime / 2;
    srand(stime);
}


static void
main_menu_loop(struct Config* config, struct Grid* grid,
    struct InterfaceData* inter_data) {

    // Compteurs & divers pour le jeu
    static const enum Position player_cycle[2] = {PLAYER_A, PLAYER_B};
    Stack* winning_moves = stack_create();
    enum Winner winner;
    enum PlayerType player_types[2];
    int turn_num, next_position, player_index;
    bool game_is_finished, there_is_a_human;

    inter_data = interface_init(config);

    // boucle des menus (fin de partie -> nouvelle partie)
    while(true) {
        // Si on quitte dans le menu
        if(interface_menu(inter_data, config) == INTERFACE_EXIT)
            break;

        grid_init(grid, config->grid_size);

        turn_num = 0;
        game_is_finished = false;
        next_position = 0;
        player_types[0] = config->player_type_a;
        player_types[1] = config->player_type_b;

        there_is_a_human =
            config->player_type_a == HUMAN ||
            config->player_type_b == HUMAN;

        interface_game_init(inter_data, config, grid);

        // boucle de jeu (pendant la partie)
        while(!game_is_finished) {

            player_index = turn_num % 2;

            // Mise à jour de l'affichage
            interface_game_update(inter_data, config, grid, turn_num);

            // On met à jour les positions disponibles
            grid_update(grid);

            // S'il ne reste plus de position jouable, problème.
            // Ceci n'est pas censé arriver.
            if(grid->available_count == 0) {
                break;
            }

            switch(player_types[player_index]) {
                case HUMAN:
                    next_position = interface_get_position(inter_data,
                        config, grid, next_position);
                break;

                case RANDOM_IA:
                    next_position = ia_random_play(grid,
                        player_cycle[player_index]);
                break;

                case MINMAX_IA:
                    next_position = ia_minmax_play(grid,
                        player_cycle[player_index]);
                break;

                case ALPHABETA_IA:
                     next_position = ia_alphabeta_play(grid,
                        player_cycle[player_index]);
                break;
            }

            // Un humain veut quitter le jeu
            if(next_position == INTERFACE_EXIT)
                break;

            // Les IA font semblant de réflechir
            if(
                player_types[player_index] == RANDOM_IA ||
                player_types[player_index] == MINMAX_IA ||
                player_types[player_index] == ALPHABETA_IA
            ) {
                msleep(there_is_a_human ?
                    IA_FAKE_DELAY : IA_FAKE_DELAY_WITHOUT_HUMAN
                );
            }

            // On modifie la grille avec le pion joué
            grid_write_by_index(grid, next_position,
                player_cycle[player_index]);

            winner = get_winner(grid, next_position, winning_moves);
            switch(winner) {
                case NOBODY:
                    if(turn_num == grid->size_square - 1) { // draw!
                        game_is_finished = true;
                        // On force la mise à jour avant l'affichage
                        interface_game_update(inter_data, config,
                            grid, turn_num);
                        interface_announce_draw(inter_data, config,
                            grid);
                        break;
                    }
                    // On continue pénards
                    turn_num++;
                break;
                case PLAYER_A_WIN:
                case PLAYER_B_WIN:
                    game_is_finished = true;
                    // estéthique : on retire les AVAILABLE
                    grid_update(grid);
                    for(int i = 0; i < grid->available_count; i++)
                        grid_write_by_index(grid, grid->available_pos[i],
                            FORBIDDEN);
                    // On force la mise à jour avant l'affichage
                    interface_game_update(inter_data, config, grid,
                        turn_num);
                    interface_announce_win(inter_data, config, grid,
                        winner, winning_moves);
                break;
            }

            // Après le premier tour, il faut passer toute la grille à FORBIDDEN
            // Attention, juste après le premier tour, turn_num vaut déjà 1.
            if(turn_num == 1) {
                for(int index = 0; index < grid->size_square; index++) {
                    if(index != next_position)
                        grid_write_by_index(grid, index, FORBIDDEN);
                }
            }

            /*
            Mise à jour des positions disponibles.
            Il suffit de rendre AVAILABLE les cases FORBIDDEN dans le carré
            (8 cases) autour de la position tout juste jouée. Attention aux
            bords et coins !
            Principe : les indices des 8 cases potentielles sont stockées dans
            cells, en ligne et colonne.
            On regarde alors quelles positions sont potentiellement modifiables
            en excluant les bords (indices négatifs ou supérieurs à grid->size).
            Une fois ce filtrage fait, on passe sur ces cases et si c'est
            FORBIDDEN, on passe à AVAILABLE.
            Schéma des indices de cells :
              0 1 2
              3 # 4
              5 6 7
            */
            int
                line = next_position / grid->size,
                col = next_position % grid->size;
            int cells[8][2];

            CELL_UP(cells, 0, line - 1, col - 1);
            CELL_UP(cells, 1, line - 1, col    );
            CELL_UP(cells, 2, line - 1, col + 1);
            CELL_UP(cells, 3, line    , col - 1);
            CELL_UP(cells, 4, line    , col + 1);
            CELL_UP(cells, 5, line + 1, col - 1);
            CELL_UP(cells, 6, line + 1, col    );
            CELL_UP(cells, 7, line + 1, col + 1);

            // On boucle sur les cases modifiables, on exclut les bords
            // (indices négatifs ou trop grands)
            int index;
            for(int i = 0; i < 8; i++) {
                if(
                    cells[i][0] >= 0 &&
                    cells[i][1] >= 0 &&
                    cells[i][0] < grid->size &&
                    cells[i][1] < grid->size
                ) {
                    index = cells[i][0] * grid->size + cells[i][1];
                    if(grid_read_by_index(grid, index) == FORBIDDEN)
                        grid_write_by_index(grid, index, AVAILABLE);
                }
            }
        }
    }

    interface_finish(inter_data);
    stack_destroy(winning_moves);
}


int
main(int argc, char const *argv[]) {
    random_seed();

    // Structures de base
    struct Config config;
    struct Grid grid;
    struct InterfaceData* inter_data;

    // Configuration par défaut
    config.grid_size = CONF_DEFAULT_SIZE;
    config.player_type_a = CONF_DEFAULT_PLAYER_A;
    config.player_type_b = CONF_DEFAULT_PLAYER_B;

    main_menu_loop(&config, &grid, inter_data);

    return 0;
}

#include "grid.h"

#include <stdbool.h>

#include "const.h"


void
grid_init(struct Grid* grid, int size) {
    grid->size = size;
    grid->size_square = size * size;
    grid->available_count = grid->size_square;

    for(int index = 0; index < grid->size_square; index++) {
        grid->available_pos[index] = index;
        grid_write_by_index(grid, index, AVAILABLE);
    }
}


void
grid_update(struct Grid* grid) {
    int available_pos_count = 0;
    for(int index = 0; index < grid->size_square; index++) {
        if(grid_read_by_index(grid, index) == AVAILABLE) {
            grid->available_pos[available_pos_count++] = index;
        }
    }
    grid->available_count = available_pos_count;
}


// Accède à l'indice (line, column) de la grille donnée
enum Position
grid_read_by_coords(struct Grid* grid, int line, int column) {
    if(0 <= line && line < grid->size && 0 <= column && column < grid->size)
        return grid_read_by_index(grid, line * grid->size + column);
    else
        return FORBIDDEN;
}


// Accède à l'indice "plat" de la grille donnée
enum Position
grid_read_by_index(struct Grid* grid, int index) {
    if(0 <= index && index < grid->size_square)
        return grid->data[index];
    else
        return FORBIDDEN;
}


// Ecrit la valeur à l'indice (line, column) de la grille donnée
void
grid_write_by_coords(struct Grid* grid, int line, int col, enum Position pos) {
    grid_write_by_index(grid, line * grid->size + col, pos);
}


// Ecrit la valeur à l'indice "plat" de la grille donnée
void
grid_write_by_index(struct Grid* grid, int index, enum Position pos) {
    grid->data[index] = pos;
}


// Copie la grille from dans dest
void
grid_copy(struct Grid* dest, struct Grid* from) {
    for(int i = 0; i < from->size_square; i++){
        grid_write_by_index(dest, i, grid_read_by_index(from, i));
    }
}

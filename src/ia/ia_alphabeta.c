#include "ia_minmax.h"

#include "../grid.h"
#include "../const.h"
#include "../utils.h"
#include "../stack.h"

#define MAX_DEPTH 3


int
ia_alphabeta_play_rec(struct Grid* grid, enum Position whoami, int depth,
    int *index_to_play, int max_depth, Stack *rand_pile, bool *continue_rec,
    int a, int b) {

    int cur, save, alpha, beta;

    if(depth < max_depth) { //descente dans l'arbre minmax jusqua max_depth
        if(depth >= 1)
            // la depth 1 correspond toujours à la décision du premier joueur, switch pour la suite
            whoami = switch_player(whoami);

        // on calcule le score de la grille en jouant la
        if(depth % 2 != max_depth % 2) {
            alpha = -MAX_VAL;

            //pour tous les fils qui correspondent aux grilles ou on va jouer dans une case dispo
            for(int i = 0; i < grid->available_count; i++) {
                int save=grid->available_pos[i];
                grid_write_by_index(grid, save, whoami);//on joue dans cette case dispo
                grid_update(grid);
                cur = ia_alphabeta_play_rec(grid, whoami, depth + 1,
                    index_to_play, max_depth, rand_pile, continue_rec,
                    max_int(a, alpha), b
                );
                grid_write_by_index(grid, save, AVAILABLE);
                grid_update(grid);

                if(alpha < cur) { // optimisation de mon score
                    stack_clear(rand_pile);
                    if(cur == MAX_VAL)
                        *continue_rec = true;
                    alpha = cur;
                    *index_to_play = save;
                } else if(alpha == cur) // utilisation pile pour choix aleatoire
                    stack_push(rand_pile, save);

                if(alpha >= b) // élagage
                    return alpha;
            }
            if(!stack_is_empty(rand_pile)) {
                *index_to_play = stack_random_elem(rand_pile);
                stack_clear(rand_pile);
            }

            return alpha;
        } else {
            beta = MAX_VAL;
            // pour tous les fils qui correspondent aux grilles ou on va jouer dans une case dispo
            for(int i=0; i < grid->available_count; i++) {
                save = grid->available_pos[i];
                grid_write_by_index(grid, save, whoami); // on joue dans cette case dispo
                grid_update(grid);
                cur = ia_alphabeta_play_rec(grid, whoami, depth + 1,
                    index_to_play, max_depth, rand_pile, continue_rec, a,
                    min_int(beta, b)
                );
                grid_write_by_index(grid, save, AVAILABLE);
                grid_update(grid);

                if(beta > cur) {
                    stack_clear(rand_pile);
                    *index_to_play = save;
                    beta = cur;
                } else if(beta == cur)
                    // utilisation de la pile pour choix aléatoire
                    stack_push(rand_pile, save);

                if(a >= beta)
                    return beta;
            }

            if(!stack_is_empty(rand_pile)) {
                *index_to_play = stack_random_elem(rand_pile);
                stack_clear(rand_pile);
            }

            return beta;
        }

    } else { // evaluation effective du score des grilles car max_depth atteint
        cur = ia_minmax_evaluation(grid, whoami);
        return cur;
    }
}


int
ia_alphabeta_play(struct Grid* grid, enum Position whoami) {
    Stack* rand_pile;
    rand_pile = stack_create();
    struct Grid copy_grid;
    grid_init(&copy_grid, grid->size);
    grid_copy(&copy_grid, grid);
    grid_update(&copy_grid);

    int score = -MAX_VAL;
    int index_to_play = copy_grid.available_pos[0];
    int max_depth = 1;
    bool continue_rec = false;
    int a = -MAX_VAL - 1;
    int b = MAX_VAL + 1;

    // grille vide, premier coup à jouer, choix aléatoire
    if(copy_grid.available_count == copy_grid.size_square)
        return random_positive(copy_grid.size_square - 1);

    // Profondeur suivante tant que coup gagnant non detecte et profondeur non max
    while(!continue_rec && max_depth <= 2) {
        score = (max_depth % 2) * MAX_VAL;
        index_to_play = copy_grid.available_pos[0];
        // donne index_to_play
        score = ia_minmax_play_rec(&copy_grid, whoami, 0, &score,
            &index_to_play, max_depth, rand_pile, &continue_rec);
        max_depth++;
    }

    while(!continue_rec && max_depth <= MAX_DEPTH){
        a = -MAX_VAL - 1;
        b = MAX_VAL + 1;
        index_to_play = copy_grid.available_pos[0];
        // donne index_to_play
        score = ia_alphabeta_play_rec(&copy_grid, whoami, 0, &index_to_play,
            max_depth, rand_pile, &continue_rec, a, b);
        max_depth++;
    }

    stack_destroy(rand_pile);
    return index_to_play;
}

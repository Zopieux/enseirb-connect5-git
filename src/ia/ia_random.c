#include "../utils.h"

#include "../grid.h"

/*
Retourne la position à jouer.
Le moteur assure qu'il est encore possible de jouer, ie. cette fonction
n'est appelée que si un pion peut encore être posé.
*/
int
ia_random_play(const struct Grid* grid, enum Position whoami) {
    return grid->available_pos[random_positive(grid->available_count - 1)];
}

#ifndef IA_MINMAX_H_
#define IA_MINMAX_H_

#include "../const.h"
#include "../grid.h"
#include "../stack.h"

#define MAX_VAL 1024

enum Alignment {LINE, COLUMN, DIAG_ASC, DIAG_DESC, ALIGNMENT_LAST};

// Donne un score à une sous grille 5*5
int tiny_eval(struct Grid* grid, int index, enum Position whoami);

// Donne un score à une grille
int ia_minmax_evaluation(struct Grid* grid, enum Position whoami);

// Change de joueur
enum Position switch_player(enum Position whoami);

// Algo minmax (récursif)
int ia_minmax_play_rec(struct Grid* grid, enum Position whoami, int depth,
    int *score, int *index_to_play, int max_depth, Stack* rand_pile,
    bool *continue_rec);

#endif

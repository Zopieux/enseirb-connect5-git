#include "ia_minmax.h"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

#include "../const.h"
#include "../grid.h"
#include "../stack.h"
#include "../utils.h"

#define MAX_DEPTH 2

// Donne un score à une sous grille 5*5
int
tiny_eval(struct Grid* grid, int index, enum Position whoami) {
    int score = 0;
    int T[ALIGNMENT_LAST] = {0, 0, 0, 0};
    int boolean[ALIGNMENT_LAST] = {0, 0, 0, 0};
    enum Position cur_player = grid_read_by_index(grid, index);

    if(cur_player == PLAYER_A || cur_player == PLAYER_B) {
        for(int i = 0; i < ALIGNMENT_LAST; i++) {
            T[i] = 1;
            boolean[i] |= 1 << cur_player;
        }
    }

    for(int i = 1; i <= 2; i++) {
        for(int j = -1; j <= 1; j++) {
            for(int k = -1; k <= 1; k++) {
                cur_player = grid_read_by_coords(grid,
                    index / grid->size + j * i,
                    index % grid->size + k * i
                );
                if(cur_player == PLAYER_A || cur_player == PLAYER_B) {
                    if(j == -1 && k == -1 || j == 1 && k == 1) {
                        T[DIAG_DESC] += 1;
                        boolean[DIAG_DESC] |= 1 << cur_player;
                        }
                    else if(j == -1 && k == 1 || j == 1 && k == -1) {
                        T[DIAG_ASC] += 1;
                        boolean[DIAG_ASC] |= 1 << cur_player;
                    }
                    else if(j == 0 && k == -1 || j ==0 && k == 1) {
                        T[LINE] += 1;
                        boolean[LINE] |= 1 << cur_player;
                    }
                    else if(j == -1 && k == 0 || j == 1 && k == 0) {
                        T[COLUMN] += 1;
                        boolean[COLUMN] |= 1 << cur_player;
                    }
                }
            }
        }
    }

    for(int i = 0; i < ALIGNMENT_LAST; i++) {
        if(boolean[i] != ((1 << PLAYER_A) | (1 << PLAYER_B))) {
            score = max_int(score, (1 << T[i]));
            if(boolean[i] != 1 << whoami)
                score = -score;
        }
    }

    return score;
}


// Donne un score à une grille.
int
ia_minmax_evaluation(struct Grid* grid, enum Position whoami) {
    int evaluation_max = 0, evaluation_min = 0, evaluation_cur = 0;

    for(int i = 0; i < grid->size_square; i++) {
        evaluation_cur = tiny_eval(grid, i, whoami);
        evaluation_max = max_int(evaluation_max, evaluation_cur);
        evaluation_min = min_int(evaluation_min, evaluation_cur);
    }

    if(abs_int(evaluation_max) >= abs_int(evaluation_min))
        return evaluation_max * evaluation_max;
    else
        return - evaluation_min * evaluation_min;
}

// Change de joueur
enum Position
switch_player(enum Position whoami) {
    return (whoami == PLAYER_A) ? PLAYER_B : PLAYER_A;
}

// Modifie l'indice en entrée qui indiquera où jouer en fonction des scores des
// grilles possibles(algo minmax)
int
ia_minmax_play_rec(struct Grid* grid, enum Position whoami, int depth,
    int *score, int *index_to_play, int max_depth, Stack *rand_pile,
    bool *continue_rec
) {

    int cur, save;

    if(depth < max_depth) { // descente dans l'arbre minmax jusqua max_depth
          if(depth>=1)
            whoami=switch_player(whoami);//la depth 1 correspond toujours à la décision du premier joueur, switch pour la suite

        for(int i = 0; i < grid->available_count; i++) { // pour tous les fils qui correspondent aux grilles ou on va jouer dans une case dispo
            save = grid->available_pos[i];
            grid_write_by_index(grid, save, whoami); // on joue dans cette case dispo
            grid_update(grid);
            cur = ia_minmax_play_rec(grid, whoami, depth + 1, &cur,
                index_to_play, max_depth, rand_pile, continue_rec);
            grid_write_by_index(grid, save, AVAILABLE);
            grid_update(grid);

            // on calcule le score de la grille en jouant la
            if(depth % 2 != max_depth % 2) {
                if(cur == MAX_VAL && max_depth <= 2) {
                    stack_clear(rand_pile);
                    *index_to_play = save;
                    *continue_rec = true;
                    return MAX_VAL;
                } else if(*score < cur) { // optimisation de mon score
                    stack_clear(rand_pile);
                    *score = cur;
                    *index_to_play = save;
                } else if(*score == cur) // utilisation pile pour choix aleatoire
                    stack_push(rand_pile,save);

            } else {
                if(*score > cur) { // minimisation score adverse
                    stack_clear(rand_pile);
                    *score = cur;
                    *index_to_play = save;
                } else if(*score == cur) // utilisation pile pour choix aleatoire
                    stack_push(rand_pile,save);
            }
        }
    } else {
        // evaluation effective du score des grilles car max_depth atteint
        cur = ia_minmax_evaluation(grid, whoami);
        return cur;
    }

    // choix aleatoire lorsque plusieurs cases ont le meilleur score
    if(!stack_is_empty(rand_pile)) {
        *index_to_play = stack_random_elem(rand_pile);
        stack_clear(rand_pile);
    }

    return *score;
}


/*
Retourne la position à jouer.
Le moteur assure qu'il est encore possible de jouer, ie. cette fonction
n'est appelée que si un pion peut encore être posé.
*/
int
ia_minmax_play(struct Grid* grid, enum Position whoami) {
    Stack *rand_pile;
    rand_pile = stack_create();
    struct Grid copied_grid;
    grid_init(&copied_grid, grid->size);
    grid_copy(&copied_grid, grid);
    grid_update(&copied_grid);

    int score = -MAX_VAL;
    int index_to_play = copied_grid.available_pos[0];
    int max_depth = 1;
    bool continue_rec = false;

    // grille vide, premier coup à jouer, choix aléatoire
    if(copied_grid.available_count == copied_grid.size_square)
        return random_positive(copied_grid.size_square - 1);

    // Profondeur suivante tant que coup gagnant non detecte et profondeur non max
    while(!continue_rec && max_depth <= MAX_DEPTH) {
        score = (max_depth % 2) * MAX_VAL;
        index_to_play = copied_grid.available_pos[0];
        score = ia_minmax_play_rec(&copied_grid, whoami, 0, &score,
            &index_to_play, max_depth, rand_pile, &continue_rec);
        max_depth++;
    }

    stack_destroy(rand_pile);
    return index_to_play;
}

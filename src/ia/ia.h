#ifndef IA_H_
#define IA_H_

#include "../const.h"
#include "../grid.h"

/*
Arguments :
  - grille
  - identité de l'IA au seins du jeu (pour qu'elle se reconnaisse, si besoin)
*/

int ia_random_play(const struct Grid*, enum Position);
int ia_minmax_play(const struct Grid*, enum Position);
int ia_alphabeta_play(const struct Grid*, enum Position);

#endif

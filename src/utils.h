/*
Fonctions-outils.
*/

#ifndef UTILS_H_
#define UTILS_H_

// Valeur absolue entière
int abs_int(int);

// Max de deux entiers
int max_int(int, int);

// Min de deux entirers
int min_int(int, int);

// Retourne un entier dans [|min, max|]
int random_int(int min, int max);

// Retourne un entier dans [|0, max|]
int random_positive(int max);

// Met le programme en pause pendant msec micro-secondes
void msleep(unsigned long msec);

#endif

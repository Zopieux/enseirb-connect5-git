/*
Interface textuelle simplissime, en console, *sans* la librairie ncurses.
Ceci implante l'API définie dans interface.h
*/

#include "interface.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "../const.h"
#include "../grid.h"
#include "../utils.h"

#define OUTBUFFER stdout
#define RAW_STR_INVALID "Cette valeur est incorrecte. Corrigez."

#define GRID_EMPTY ' '
#define GRID_AVAILABLE '*'
#define MARKER_FOR_PLAYER_A 'O'
#define MARKER_FOR_PLAYER_B 'X'

#define ANSWER_MAX_LEN 3
#define ANSWER_MAX_LEN_ "3"
#define ANSWER_YES_LEN 4
#define ANSWER_YES {"oui", "o", "ok", "yes"}
#define PROMPT_CHARS "\\_o> "


// Données utiles pour cette interface
struct InterfaceData {
    bool is_first_game;
};


static void
empty_buffer() {
    // Astuce : permet de vider le tampon après un scanf de string
    scanf("%*[^\n]");
    getchar();
}


// Vérifie si un string est "oui" (& cie.)
static bool
answer_is_yes(char* answer) {
    static const char* yes_answers[ANSWER_YES_LEN] = ANSWER_YES;

    for(int i = 0; i < ANSWER_YES_LEN; i++) {
        if(strcmp(answer, yes_answers[i]) == 0)
            return true;
    }
    return false;
}


// Affiche la grille
static void
raw_draw_grid(struct Grid* grid) {
    enum Position pos;

    fprintf(OUTBUFFER, "   "); // marge pour marqueur lignes
    for(int i = 0; i < grid->size; i++)
        fprintf(OUTBUFFER, "%2d ", i + 1);
    fprintf(OUTBUFFER, "\n");

    for(int line = 0; line < grid->size; line++) {
        fprintf(OUTBUFFER, "%2d ", line + 1);
        for(int col = 0; col < grid->size; col++) {
            pos = grid_read_by_index(grid, col * grid->size + line);
            fprintf(OUTBUFFER, " %c ",
                (pos == FORBIDDEN) ? GRID_EMPTY          :
                (pos == AVAILABLE) ? GRID_AVAILABLE      :
                (pos == PLAYER_A)  ? MARKER_FOR_PLAYER_A :
                                     MARKER_FOR_PLAYER_B
            );
        }
        fprintf(OUTBUFFER, "\n");
    }
}


// Vérifie qu'un élément appartient à un tableau
static bool
raw_is_in_array(int* array, int array_size, int elem) {
    for(int i = 0; i < array_size; i++)
        if(elem == array[i])
            return true;
    return false;
}


/*
Affiche la grille du gagnant, en ignorant les pions qui ne sont pas
l'alignement gagnant.
Cette fonction semble avoir une complexité déraisonnable car on vérifie n² fois
si le pion fait partie de l'alignement gagnant, soit 5 * n² au mieux, 9 * n²
au pire. Soit une complexité quadratique, ce qui est de toute façon le minimum
car il faut itérer sur toute la grille pour l'affichage. Donc au final, on ne
s'en sort pas trop mal !
*/
static void
raw_draw_grid_win(struct Grid* grid, Stack* winning_moves) {
    int pos_int;

    int winning_moves_array[GRID_SIZE_MAX];
    int win_count = 0;
    while(!stack_is_empty(winning_moves))
        winning_moves_array[win_count++] = stack_pop(winning_moves);

    fprintf(OUTBUFFER, "   "); // marge pour marqueur lignes
    for(int i = 0; i < grid->size; i++)
        fprintf(OUTBUFFER, "%2d ", i + 1);
    fprintf(OUTBUFFER, "\n");

    for(int line = 0; line < grid->size; line++) {
        fprintf(OUTBUFFER, "%2d ", line + 1);
        for(int col = 0; col < grid->size; col++) {
            pos_int = col * grid->size + line;
            // si c'est gagnant, on affiche
            if(raw_is_in_array(winning_moves_array, win_count, pos_int)) {
                fprintf(OUTBUFFER, " %c ",
                    (grid_read_by_index(grid, pos_int) == PLAYER_A) ?
                        MARKER_FOR_PLAYER_A :
                        MARKER_FOR_PLAYER_B
                );
            } else
                fprintf(OUTBUFFER, "   ");
        }
        fprintf(OUTBUFFER, "\n");
    }
}


struct InterfaceData*
interface_init(struct Config* config) {
    struct InterfaceData* inter_data = malloc(sizeof(struct InterfaceData));

    inter_data->is_first_game = true;

    fprintf(OUTBUFFER, "Bienvenue dans Puissance 5 !\n");
    fprintf(OUTBUFFER, "=============================\n\n");
    fprintf(OUTBUFFER, "Pour quitter, pressez Ctrl+C à tout moment.\n\n");

    return inter_data;
};


int
interface_menu(struct InterfaceData* inter_data, struct Config* config) {
    char* player_type_labels[PLAYER_TYPE_LAST] = {
        STR_PLAYER_HUMAN, STR_RANDOM_IA, STR_MINMAX_IA, STR_ALPHABETA_IA
    };

    char answer_buff[ANSWER_MAX_LEN];
    bool ask_config = true;

    fprintf(OUTBUFFER, "Une nouvelle partie commence.\n");
    fprintf(OUTBUFFER, "=============================\n");

    if(!(inter_data->is_first_game)) {
        // Demande si on veut réutiliser la même configuration
        fprintf(OUTBUFFER, "Rappel : pour quitter, pressez Ctrl+C à tout moment.\n\n");
        fprintf(OUTBUFFER, "Voulez-vous garder la même configuration de jeu ?\n" PROMPT_CHARS);
        scanf("%" ANSWER_MAX_LEN_ "s", answer_buff);
        empty_buffer();

        ask_config = !answer_is_yes(answer_buff);

    } else
        inter_data->is_first_game = false;

    if(!ask_config)
        return INTERFACE_CONTINUE;

    fprintf(OUTBUFFER, "Quelle est la taille de la grille (entre %d et %d) ?\n"
        PROMPT_CHARS,
        GRID_SIZE_MIN, GRID_SIZE_MAX);
    while(true) {
        scanf("%d", &(config->grid_size));
        empty_buffer();
        if(config->grid_size >= GRID_SIZE_MIN && config->grid_size <= GRID_SIZE_MAX)
            break;
        fprintf(OUTBUFFER, RAW_STR_INVALID PROMPT_CHARS);
    }

    fprintf(OUTBUFFER, "La taille de la grille est %d.\n\n", config->grid_size);

    fprintf(OUTBUFFER, "Quel est le type du joueur A ?\nChoix possibles :\n");
    for(int i = 0; i < PLAYER_TYPE_LAST; i++) {
        fprintf(OUTBUFFER, "  - %d. %s\n", i + 1, player_type_labels[i]);
    }
    fprintf(OUTBUFFER, PROMPT_CHARS);
    while(true) {
        scanf("%d", &(config->player_type_a));
        empty_buffer();
        (config->player_type_a)--; // car on commence à 1 chez les humains
        if(config->player_type_a >= 0 && config->player_type_a < PLAYER_TYPE_LAST)
            break;

        fprintf(OUTBUFFER, RAW_STR_INVALID "\n" PROMPT_CHARS);
    }

    fprintf(OUTBUFFER, "Le joueur A est %s.\n\n", player_type_labels[config->player_type_a]);

    fprintf(OUTBUFFER, "Quel est le type du joueur B ?\nChoix possibles :\n");
    for(int i = 0; i < PLAYER_TYPE_LAST; i++) {
        fprintf(OUTBUFFER, "  - %d. %s\n", i + 1, player_type_labels[i]);
    }
    fprintf(OUTBUFFER, PROMPT_CHARS);
    while(true) {
        scanf("%d", &(config->player_type_b));
        empty_buffer();
        (config->player_type_b)--; // car on commence à 1 chez les humains
        if(config->player_type_b >= 0 && config->player_type_b < PLAYER_TYPE_LAST)
            break;

        fprintf(OUTBUFFER, RAW_STR_INVALID "\n> ");
    }

    fprintf(OUTBUFFER, "Le joueur B est %s.\n\n", player_type_labels[config->player_type_b]);

    return INTERFACE_CONTINUE;
}

void interface_game_init(struct InterfaceData* inter_data,
    struct Config* config, struct Grid* grid) {
    fprintf(OUTBUFFER, "\n\nLa partie commence !\n\n");
}

void interface_game_update(struct InterfaceData* inter_data,
    struct Config* config, struct Grid* grid, int turn_num) {
    raw_draw_grid(grid);
    fprintf(OUTBUFFER, "\n");
    fprintf(OUTBUFFER, "Au tour de %s ( %c ) :\n",
        turn_num % 2 == 0 ? STR_PLAYER_A : STR_PLAYER_B,
        turn_num % 2 == 0 ? MARKER_FOR_PLAYER_A : MARKER_FOR_PLAYER_B
    );
}

int interface_get_position(struct InterfaceData* inter_data,
    struct Config* config, struct Grid* grid, int last_played_position) {

    int pos_line;
    int pos_col;
    int pos;

    fprintf(OUTBUFFER, "\n");
    while(true) {
        fprintf(OUTBUFFER, "  Ligne (1 à %d) " PROMPT_CHARS, grid->size);
        scanf("%d", &pos_line);
        empty_buffer();
        // fprintf(OUTBUFFER, "\n");
        fprintf(OUTBUFFER, "Colonne (1 à %d) " PROMPT_CHARS, grid->size);
        scanf("%d", &pos_col);
        empty_buffer();
        fprintf(OUTBUFFER, "\n");
        if(pos_line < 1 || pos_line > grid->size || pos_col < 1 || pos_col > grid->size) {
            fprintf(OUTBUFFER, RAW_STR_INVALID "\n");
            continue;
        }
        pos = (pos_col - 1) * grid->size + pos_line - 1;
        if(pos >= 0 && pos < grid->size_square && grid_read_by_index(grid, pos) == AVAILABLE)
            break;
        fprintf(OUTBUFFER, RAW_STR_INVALID "\n");
    }

    return pos;
}

void interface_announce_win(struct InterfaceData* inter_data,
    struct Config* config, struct Grid* grid, enum Position winner,
    Stack* winning_moves) {
    const char* winner_str = winner == PLAYER_A ? STR_PLAYER_A : STR_PLAYER_B;

    raw_draw_grid_win(grid, winning_moves);

    fprintf(OUTBUFFER, "\n" STR_GAME_WIN "\n\n", winner_str);
}

void interface_announce_draw(struct InterfaceData* inter_data,
    struct Config* config, struct Grid* grid) {

    raw_draw_grid(grid);
    fprintf(OUTBUFFER, "\n" STR_GAME_DRAW "\n\n");
}

void interface_finish(struct InterfaceData* inter_data) {

}

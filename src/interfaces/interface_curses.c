/*
Interface textuelle, en console, *avec* la librairie ncurses.
Ceci implante l'API définie dans interface.h
*/

#include "interface.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <ncurses.h>

#include "../const.h"
#include "../grid.h"
#include "../utils.h"

/*
true:  le curseur de séléction (humain) est restreint aux cases disponibles
false: le curseur est libre (si on essaye de jouer sur une position prise,
       on ignore, rien ne se passe)
*/
#define CURSOR_RESTRAINED_DEFAULT true
#define CURSOR_RESTRAINED_TOGGLE_KEY 'r'

#define MENU_WIDTH 80
#define MENU_HEIGHT 23
#define MENU_MARGIN 4
#define MENU_MAIN_OFFSET_Y 3
#define MENU_TICKRATE 300

#define MENU_CONF_INNER_LEN "25"

#define MENU_CONF_INT_FMT "- %" MENU_CONF_INNER_LEN "d +"
#define MENU_CONF_ENUM_FMT "- %" MENU_CONF_INNER_LEN "s +"
#define MENU_CONF_LEN 29 // MENU_CONF_INNER_LEN + 4
#define MENU_OFFSET_Y 15
#define MENU_FRAMES 3
#define MENU_MARKER_LEFT "> ="
#define MENU_MARKER_RIGHT "< ="
#define MENU_MARKER_MARGIN 5
#define MENU_POSITIONS {0, 3, 4, 5, 7}
#define MENU_LENGTH 5

// Nombre de lignes pour l'affichage contextuel sous la grille
#define GRID_MARGIN_BOTTOM 7

#define GRID_EMPTY          "   "
#define GRID_AVAILABLE      " . "
#define GRID_CURSOR         "[.]"
#define MARKER_FOR_PLAYER_A " O "
#define MARKER_FOR_PLAYER_B " X "
#define STR_CURSOR "Curseur (%s)"
#define STR_CURSOR_RESTRAINED "restreint"
#define       STR_CURSOR_FREE "  libre  "

#define COLOR_FOR_TITLE 1
#define COLOR_FOR_CURSOR 2
#define COLOR_FOR_BORDERS 3
#define COLOR_FOR_WINNER 4


// Données utiles pour cette interface ncurses
struct InterfaceData {
    WINDOW* main_win;
    WINDOW* grid_pad;
    int main_width; int main_height;
    int grid_width; int grid_height;
    bool cursor_restrained;
};


static void
console_erase_box(WINDOW* win, int y1, int x1, int y2, int x2) {
    for(int y = y1; y <= y2; y++)
        for(int x = x1; x <= x2; x++)
            mvwaddch(win, y, x, ' ');
}


static void
console_draw_buffer(WINDOW* win, int y, int x, char buffer[]) {
    wmove(win, y, x);
    for(int i = 0; i < strlen(buffer); i++) {
        if(buffer[i] == '\n')
            wmove(win, ++y, x);
        else
            waddch(win, buffer[i]);
    }
}


static void
console_print_menu_conf_int(WINDOW* win, int var, int y, int x, char text_label[]) {
    int rows, cols;
    getmaxyx(win, rows, cols);
    // label
    mvwprintw(win, y, x, text_label);
    // valeur
    mvwprintw(win, y, cols - MENU_CONF_LEN - MENU_MARGIN, MENU_CONF_INT_FMT, var);
}


static void
console_print_menu_conf_enum(WINDOW* win, int index, int y, int x,
        char text_label[], char* value_labels[]) {
    int rows, cols;
    getmaxyx(win, rows, cols);
    // label
    mvwprintw(win, y, x, text_label);
    // valeur
    mvwprintw(win, y, cols - MENU_CONF_LEN - MENU_MARGIN, MENU_CONF_ENUM_FMT,
        value_labels[index]);
}


static void
console_draw_grid(struct InterfaceData* inter_data,    struct Grid* grid) {
    // Layout
    box(inter_data->grid_pad, 0, 0);
    for(int i = 1; i < grid->size; i++) {
        // horizontal borders
        mvwaddch(inter_data->grid_pad, 0, 4 * i, ACS_TTEE);
        mvwaddch(inter_data->grid_pad, inter_data->grid_height - 1, 4 * i,
            ACS_BTEE);
        // vertical borders
        mvwaddch(inter_data->grid_pad, 2 * i, 0, ACS_LTEE);
        mvwaddch(inter_data->grid_pad, 2 * i, inter_data->grid_width - 1,
            ACS_RTEE);

        mvwhline(inter_data->grid_pad, 2 * i, 1, ACS_HLINE,
            inter_data->grid_width - 2);
        mvwvline(inter_data->grid_pad, 1, 4 * i, ACS_VLINE,
            inter_data->grid_height - 2);
    }
    for(int i = 1; i < grid->size; i++)
        for(int j = 1; j < grid->size; j++)
            mvwaddch(inter_data->grid_pad, 2 * j, 4 * i, ACS_PLUS);

    // Contenu
    enum Position pos;
    for(int index = 0; index < grid->size_square; index++) {
        pos = grid_read_by_index(grid, index);

        mvwprintw(inter_data->grid_pad,
            2 * (index % grid->size) + 1, // y
            4 * (index / grid->size) + 1, // x
            (pos == FORBIDDEN) ? GRID_EMPTY          :
            (pos == AVAILABLE) ? GRID_AVAILABLE      :
            (pos == PLAYER_A)  ? MARKER_FOR_PLAYER_A :
                                 MARKER_FOR_PLAYER_B
        );
    }
}


int
interface_menu(struct InterfaceData* inter_data, struct Config* config) {
    clear();
    refresh();

    WINDOW* main_menu;
    WINDOW* menu_win;

    main_menu = newwin(
        MENU_HEIGHT,
        MENU_WIDTH,
        MENU_MAIN_OFFSET_Y,
        (COLS - MENU_WIDTH) / 2
    );

    menu_win = newwin(
        MENU_HEIGHT - MENU_OFFSET_Y,
        MENU_WIDTH - MENU_MARKER_MARGIN * 2,
        MENU_OFFSET_Y,
        (COLS - MENU_WIDTH + MENU_MARKER_MARGIN * 2) / 2
    );

    wattron(main_menu, COLOR_PAIR(COLOR_FOR_BORDERS));
    box(main_menu, 0, 0);
    wattroff(main_menu, COLOR_PAIR(COLOR_FOR_BORDERS));

    // Titre et sous-titre
    wattron(main_menu, COLOR_PAIR(COLOR_FOR_TITLE));
    console_draw_buffer(main_menu, 2, (MENU_WIDTH - STR_BIG_TITLE_WIDTH) / 2,
        STR_BIG_TITLE);
    wattroff(main_menu, COLOR_PAIR(COLOR_FOR_TITLE));

    mvwprintw(main_menu, 8, (MENU_WIDTH - strlen(STR_SUBTITLE)) / 2,
        STR_SUBTITLE);
    mvwprintw(main_menu, 9, (MENU_WIDTH - strlen(STR_COPYRIGHT)) / 2,
        STR_COPYRIGHT);
    wrefresh(main_menu);

    timeout(MENU_TICKRATE); // getch() attend la durée MENU_TICKRATE avant de return

    bool ctn = true;
    int return_value = INTERFACE_CONTINUE;
    int ch;
    int current_item = 0;
    int frame_count = 0;
    int menu_pos[MENU_LENGTH] = MENU_POSITIONS;

    int
        current_type_a = config->player_type_a,
        current_type_b = config->player_type_b;

    char* player_type_labels[PLAYER_TYPE_LAST] = {
        STR_PLAYER_HUMAN, STR_RANDOM_IA, STR_MINMAX_IA, STR_ALPHABETA_IA
    };

    do {
        wclear(menu_win);

        // Options
        mvwprintw(menu_win, menu_pos[1] - 1, MENU_MARGIN, STR_OPTIONS);

        wattron(menu_win, A_BOLD);
        // Jouer !
        mvwprintw(menu_win, menu_pos[0], MENU_MARGIN, STR_PLAYNOW);


        // Taille de la grille
        console_print_menu_conf_int(menu_win, config->grid_size, menu_pos[1],
            MENU_MARGIN, STR_GRID_SIZE);

        // Joueur A
        console_print_menu_conf_enum(menu_win, current_type_a, menu_pos[2],
            MENU_MARGIN, STR_PLAYER_TYPE_A,
            player_type_labels);

        // Joueur B
        console_print_menu_conf_enum(menu_win, current_type_b, menu_pos[3],
            MENU_MARGIN, STR_PLAYER_TYPE_B,
            player_type_labels);

        // Quitter
        mvwprintw(menu_win, menu_pos[4], MENU_MARGIN, STR_QUIT);
        wattroff(menu_win, A_BOLD);

        // Affichage du pointeur à gauche de la ligne
        mvwaddch(menu_win, menu_pos[current_item], 0,
            MENU_MARKER_LEFT[frame_count]);

        // Affichage du pointeur à droite de la ligne
        mvwaddch(menu_win, menu_pos[current_item],
            MENU_WIDTH - MENU_MARKER_MARGIN * 2 - 1,
            MENU_MARKER_RIGHT[frame_count]);

        // Formatage sur la ligne en cours
        mvwchgat(menu_win, menu_pos[current_item], 0, MENU_WIDTH,
            A_BOLD | A_REVERSE,0, NULL);

        wrefresh(menu_win);

        if(++frame_count >= MENU_FRAMES) frame_count = 0;

        if((ch = getch()) != ERR) {
            switch(ch) {
                case KEY_UP:
                    if(current_item > 0)
                        current_item--;
                    else
                        current_item = MENU_LENGTH - 1;
                break;

                case KEY_DOWN:
                    if(current_item < MENU_LENGTH - 1)
                        current_item++;
                    else
                        current_item = 0;
                break;

                case '+':
                case KEY_RIGHT:
                    switch(current_item) {
                        case 1:
                            if(config->grid_size < GRID_SIZE_MAX) {
                                config->grid_size++;
                            }
                        break;
                        case 2:
                            if(++current_type_a >= PLAYER_TYPE_LAST)
                                current_type_a = 0;
                        break;
                        case 3:
                            if(++current_type_b >= PLAYER_TYPE_LAST)
                                current_type_b = 0;
                        break;
                    }
                break;

                case '-':
                case KEY_LEFT:
                    switch(current_item) {
                        case 1:
                            if(config->grid_size > GRID_SIZE_MIN)
                                config->grid_size--;
                        break;
                        case 2:
                            if(--current_type_a < 0)
                                current_type_a = PLAYER_TYPE_LAST - 1;
                        break;
                        case 3:
                            if(--current_type_b < 0)
                                current_type_b = PLAYER_TYPE_LAST - 1;
                        break;
                    }
                break;

                case '\n':
                    switch(current_item) {
                        case 0:
                            // PLAY!!!
                            ctn = false;
                        break;
                        case 4:
                            return_value = INTERFACE_EXIT;
                            ctn = false;
                        break;
                    }
                break;

                case KEY_ESCAPE:
                    return_value = INTERFACE_EXIT;
                    ctn = false;
                break;
            }
        }
    } while(ctn);

    // Mise à jour de la configuration (pour les player_types, car grid_size
    // c'est déjà fait dans la boucle)
    config->player_type_a = current_type_a;
    config->player_type_b = current_type_b;

    delwin(main_menu);
    return return_value;
}


void
interface_game_update(struct InterfaceData* inter_data, struct Config *config,
        struct Grid* grid, int turn_num) {

    // (Loto,) À qui le tour ?
    mvwprintw(inter_data->main_win, inter_data->grid_height + 2, MENU_MARGIN,
        STR_TURN_NUM, turn_num + 1
    );
    mvwprintw(inter_data->main_win,
        inter_data->grid_height + 2,
        MENU_MARGIN + sizeof(STR_TURN_NUM),
        STR_TURN_OF, (turn_num % 2 == 0) ? STR_PLAYER_A : STR_PLAYER_B,
        (turn_num % 2 == 0) ? MARKER_FOR_PLAYER_A : MARKER_FOR_PLAYER_B
    );

    wrefresh(inter_data->main_win);

    // Mise à jour de la grille
    wclear(inter_data->grid_pad);
    console_draw_grid(inter_data, grid);
    wrefresh(inter_data->grid_pad);
}


int
interface_get_position(struct InterfaceData* inter_data, struct Config* config,
        struct Grid* grid, int last_played_position) {
    int ch;

    timeout(-1); // supprime la condition de timeout sur getch

    int cur_pos = last_played_position;
    int additive = 1;
    bool do_move = false;

    while(true) {
        if(do_move || inter_data->cursor_restrained) {
            // On cherche la prochaine position disponible
            do {
                cur_pos += additive;
                if(cur_pos < 0)
                    cur_pos += grid->size_square;
                cur_pos %= grid->size_square;
            } while(inter_data->cursor_restrained &&
                grid->data[cur_pos] != AVAILABLE);
        }

        /*
        Plutôt que tout redessiner, ce qui est coûteux, on peut s'amuser à
        retenir le caractère qui était sous le curseur et le redessiner quand on
        change. Mais c'est assez lourd à implanter.
        */
        console_draw_grid(inter_data, grid);

        // Position textuelle curseur
        // 4 : pour %s et espaces
        mvwprintw(inter_data->main_win,
            inter_data->grid_height + 2,
            inter_data->main_width - MENU_MARGIN - sizeof(STR_CURSOR) + 4
                - sizeof(STR_CURSOR_RESTRAINED),
            STR_CURSOR,
            inter_data->cursor_restrained ?
                STR_CURSOR_RESTRAINED :
                STR_CURSOR_FREE
        );
        // 10 = sizeof("[.] xx, yy"")
        mvwprintw(inter_data->main_win,
            inter_data->grid_height + 3,
            inter_data->main_width - MENU_MARGIN - 10,
            "[.] %2d, %2d",
            cur_pos % grid->size + 1, cur_pos / grid->size + 1
        );

        // affiche le curseur
        wattron(inter_data->grid_pad, A_BOLD | COLOR_PAIR(COLOR_FOR_CURSOR));
        mvwprintw(inter_data->grid_pad,
            2 * (cur_pos % grid->size) + 1,
            4 * (cur_pos / grid->size) + 1,
            GRID_CURSOR
        );

        wrefresh(inter_data->main_win);
        wattroff(inter_data->grid_pad, A_BOLD | COLOR_PAIR(COLOR_FOR_CURSOR));

        wrefresh(inter_data->grid_pad);
        wclear(inter_data->grid_pad);

        ch = getch();

        switch(ch) {
            case CURSOR_RESTRAINED_TOGGLE_KEY:
                // on toggle le mode restreint
                additive = 0;
                inter_data->cursor_restrained = !inter_data->cursor_restrained;
                if(inter_data->cursor_restrained) {
                    /*
                    si on remet en restreint, il faut positionner le curseur sur
                    une case libre !
                    On est assuré par engine que available_pos[0] existe.
                    */
                    cur_pos = grid->available_pos[0];
                }
            break;
            case KEY_RIGHT:
                do_move = true;
                additive = grid->size;
            break;
            case KEY_LEFT:
                do_move = true;
                additive = -grid->size;
            break;
            case KEY_UP:
                do_move = true;
                additive = -1;
            break;
            case KEY_DOWN:
                do_move = true;
                additive = 1;
            break;
            case KEY_ESCAPE:
                return INTERFACE_EXIT;
            break;
            // si le choix est fait, on redonne la main à engine
            case '\n':
                // si on est en RESTRAINED, la position est forcément dispo
                // sinon, on accepte que si c'est dispo !
                do_move = false;
                if(inter_data->cursor_restrained ||
                    grid->data[cur_pos] == AVAILABLE)
                    return cur_pos;
            break;
        }
    }
}


void
interface_announce_draw(struct InterfaceData* inter_data, struct Config* config,
    struct Grid* grid) {

    console_erase_box(inter_data->main_win,
        inter_data->grid_height + 2, MENU_MARGIN,
        inter_data->grid_height + 4, inter_data->main_width - MENU_MARGIN
    );

    mvwprintw(inter_data->main_win,
        inter_data->grid_height + 2,
        (inter_data->main_width - sizeof(STR_GAME_END)) / 2,
        STR_GAME_END
    );
    mvwprintw(inter_data->main_win,
        inter_data->grid_height + 4,
        (inter_data->main_width - sizeof(STR_GAME_DRAW)) / 2,
        STR_GAME_DRAW
    );

    wrefresh(inter_data->main_win);

    // Attente d'une pression de touche
    int ch;
    while(1) {
        ch = getch();
        if(ch != ERR && (ch == KEY_ESCAPE || ch == '\n'))
            break;
    }
}


void
interface_announce_win(struct InterfaceData* inter_data, struct Config* config,
    struct Grid* grid, enum Position winner, Stack* winning_moves) {

    const char* winner_str = winner == PLAYER_A ? STR_PLAYER_A : STR_PLAYER_B;

    console_erase_box(inter_data->main_win,
        inter_data->grid_height + 2, MENU_MARGIN,
        inter_data->grid_height + 4, inter_data->main_width - MENU_MARGIN
    );

    mvwprintw(inter_data->main_win,
        inter_data->grid_height + 2,
        (inter_data->main_width - sizeof(STR_GAME_END)) / 2,
        STR_GAME_END
    );
    mvwprintw(inter_data->main_win,
        inter_data->grid_height + 4,
        (inter_data->main_width - sizeof(STR_GAME_WIN) - 2 -
            sizeof(winner_str)) / 2,
        STR_GAME_WIN,
        winner_str
    );

    int move;
    // On met en valeur l'alignement gagnant
    while(!stack_is_empty(winning_moves)) {
        move = stack_pop(winning_moves);
        mvwchgat(inter_data->grid_pad,
            2 * (move % grid->size) + 1, // y
            4 * (move / grid->size) + 1, // x
            sizeof(MARKER_FOR_PLAYER_A) - 1, // nombre de chars affectés
            A_NORMAL, // mise en forme
            COLOR_FOR_WINNER, // couleur
            NULL
        );
    }

    wrefresh(inter_data->grid_pad);
    wrefresh(inter_data->main_win);

    // Attente d'une pression de touche
    int ch;
    while(1) {
        ch = getch();
        if(ch != ERR && (ch == KEY_ESCAPE || ch == '\n'))
            break;
    }
}


struct InterfaceData*
interface_init(struct Config* config) {
    initscr();

    if(has_colors() == FALSE) {
        endwin();
        printf("Ce terminal ne supporte pas les couleurs.\n");
        exit(1);
    }

    start_color();
    init_pair(COLOR_FOR_TITLE, COLOR_GREEN, COLOR_BLACK);
    init_pair(COLOR_FOR_CURSOR, COLOR_RED, COLOR_BLACK);
    init_pair(COLOR_FOR_BORDERS, COLOR_YELLOW, COLOR_BLACK);
    init_pair(COLOR_FOR_WINNER, COLOR_GREEN, COLOR_BLACK);

    noecho();
    cbreak();
    curs_set(0);
    keypad(stdscr, true); // active les touches spéciales (combinaisons)

    refresh();

    // Allocation dynamique pour les données spécifiques à cette interface
    struct InterfaceData* inter_data = malloc(sizeof(struct InterfaceData));
    return inter_data;
}


void
interface_game_init(struct InterfaceData* inter_data, struct Config* config,
        struct Grid* grid) {
    const int
        deco_grid_height = config->grid_size * 2 + 1,
        deco_grid_width = config->grid_size * 4 + 1;

    const int win_width = max_int(deco_grid_width + 4, MENU_WIDTH);
    const int win_height = max_int(deco_grid_height + GRID_MARGIN_BOTTOM,
        MENU_HEIGHT);

    WINDOW* main_win = newwin(
        win_height,
        win_width,
        0,
        (COLS - win_width) / 2
    );
    WINDOW* grid_pad = newwin(
        deco_grid_height,
        deco_grid_width,
        1,
        (COLS - deco_grid_width) / 2
    );

    clear();
    refresh();

    // Un peu de déco
    wattron(main_win, COLOR_PAIR(COLOR_FOR_BORDERS));
    box(main_win, 0, 0);
    wattroff(main_win, COLOR_PAIR(COLOR_FOR_BORDERS));

    refresh();

    inter_data->main_win = main_win;
    inter_data->main_width = win_width;
    inter_data->main_height = win_height;
    inter_data->grid_pad = grid_pad;
    inter_data->cursor_restrained = CURSOR_RESTRAINED_DEFAULT;
    inter_data->grid_width = deco_grid_width;
    inter_data->grid_height = deco_grid_height;
}


void
interface_finish(struct InterfaceData* inter_data) {
    free(inter_data);
    endwin();
}

/*
API pour les différentes interfaces.
À la compilation, on peut choisir quelle 'interface'.c on utilise pour implanter
cette API (cf. README).
*/

#ifndef INTERFACE_H_
#define INTERFACE_H_

#include "../const.h"
#include "../grid.h"
#include "../stack.h"

struct InterfaceData;

// Initilisation préliminaire
struct InterfaceData* interface_init(struct Config*);

// Menu de configuration de la partie
int interface_menu(struct InterfaceData*, struct Config*);

// Initilisation début de partie
void interface_game_init(struct InterfaceData*, struct Config*, struct Grid*);

// Mise à jour de l'interface à chaque tour
void interface_game_update(struct InterfaceData*, struct Config*, struct Grid*,
    int turn);

// Permet de récupérer la position du prochain pion pour un joueur humain
int interface_get_position(struct InterfaceData*, struct Config*,
    struct Grid*, int last_played_position);

// Annonce d'une victoire
void interface_announce_win(struct InterfaceData*, struct Config*,
    struct Grid*, enum Position winner, Stack* winning_moves);

// Annonce d'un match nul
void interface_announce_draw(struct InterfaceData*, struct Config*,
    struct Grid*);

// Nettoyage
void interface_finish(struct InterfaceData*);

#endif

#include "stack.h"

#include <stdlib.h>

#include "utils.h"


struct Stack {
    int size;
    int data[GRID_SIZE_MAX * GRID_SIZE_MAX];
};


Stack*
stack_create() {
    Stack* stack = malloc(sizeof(Stack));
    stack->size = 0;
    return stack;
}


Stack*
stack_push(Stack* stack, int element) {
    stack->data[stack->size++] = element;
    return stack;
}


int
stack_pop(Stack* stack) {
    int head = stack_head(stack);
    stack->size--;
    return head;
}


Stack*
stack_clear(Stack* stack) {
    stack->size = 0;
    return stack;
}


int
stack_head(Stack* stack) {
    return stack->data[stack->size - 1];
}


int stack_random_elem(Stack* stack) {
    return stack->data[random_positive(stack->size - 1)];
}


bool
stack_is_empty(Stack* stack) {
    return stack->size == 0;
}


void
stack_destroy(Stack* stack) {
    free(stack);
}

/*
Constantes et structures de données utilisées globalement.
*/

#ifndef CONST_H_
#define CONST_H_

#include <stdbool.h>

/*
Les deux premiers devraient être égaux...
ou au moins, GRID_SIZE_MIN >= PUISSANCE_N
*/
#define PUISSANCE_N 5
#define GRID_SIZE_MIN 5
/*
Arbitraire, cela évite d'afficher des grilles trop hautes, vues les limitations
d'une interface textuelle (pas de scroll)...
*/
#define GRID_SIZE_MAX 40


enum PlayerType {HUMAN, RANDOM_IA, MINMAX_IA, ALPHABETA_IA, PLAYER_TYPE_LAST};

enum Winner {NOBODY, DRAW, PLAYER_A_WIN, PLAYER_B_WIN};
enum Position {FORBIDDEN, AVAILABLE, PLAYER_A, PLAYER_B};

struct Config {
    int grid_size;
    enum PlayerType player_type_a;
    enum PlayerType player_type_b;
};


#define CONF_DEFAULT_SIZE 13
#define CONF_DEFAULT_PLAYER_A HUMAN
#define CONF_DEFAULT_PLAYER_B MINMAX_IA

// Il manque KEY_ESCAPE dans ncurses.h
#ifndef KEY_ESCAPE
#define KEY_ESCAPE 27
#endif

// Délai de réflexion artificiel pour les IA, en msecs
#define IA_FAKE_DELAY 0
// Délai de réflexion artificiel pour les IA, si aucun joueur humain, en msecs
#define IA_FAKE_DELAY_WITHOUT_HUMAN 150

#define INTERFACE_CONTINUE 0
// Une valeur négative (quelconque) est primordiale ici
#define INTERFACE_EXIT -1

// Constantes textuelles
#define STR_PLAYER_HUMAN "Humain"
#define STR_PLAYER_A "Joueur A"
#define STR_PLAYER_B "Joueur B"
#define STR_RANDOM_IA "IA aleatoire (idiote)"
#define STR_MINMAX_IA "IA min-max (intelligente)"
#define STR_ALPHABETA_IA "IA alpha-beta (rapide)"
#define STR_PLAYER_TYPE_A "   Type du joueur A"
#define STR_PLAYER_TYPE_B "   Type du joueur B"
#define STR_GRID_SIZE "   Taille de la grille"
#define STR_QUIT "Quitter Puissance 5"
#define STR_PLAYNOW "Lancer la partie !"
#define STR_OPTIONS "Options :"
#define STR_TURN_NUM "[tour %3d] "
#define STR_TURN_OF "%s (%s), à vous !" // (nom joueur, marque joueur)
#define STR_GAME_END "La partie est terminee !"
#define STR_GAME_DRAW "Desole, mais cette manche s'est finie sur un match nul."
#define STR_GAME_WIN "Felicitations %s, vous gagnez cette manche !"
// Longueur de la plus grande ligne de STR_BIG_TITLE
#define STR_BIG_TITLE_WIDTH 59
#define STR_BIG_TITLE "    ____        _                                    ______\n\
   / __ \\__  __(_)_____________ _____  ________     / ____/\n\
  / /_/ / / / / / ___/ ___/ __ `/ __ \\/ ___/ _ \\   /___ \\ \n\
 / ____/ /_/ / (__  |__  ) /_/ / / / / /__/  __/  ____/ /\n\
/_/    \\__,_/_/____/____/\\__,_/_/ /_/\\___/\\___/  /_____/"
#define STR_SUBTITLE "Un jeu de ~ldauphin, ~amacabies et ~esagardia"
#define STR_COPYRIGHT "Copyright (c) ENSEIRB 2012"

#endif
